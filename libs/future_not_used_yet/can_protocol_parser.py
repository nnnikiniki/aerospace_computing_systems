import json
import os
from dataclasses import InitVar, dataclass, field
from pathlib import Path
from typing import Dict, List, Optional, Union

PROTOCOL_FILENAME = Path("can_protocol_config.json")

# TODO надо обрабатывать ещё 2-й и 3-й биты, а не только 1-й (extended)
# TODO добавить docstring ко всем функциям

# TODO в возвращаемые параметры добавить флаг успех/неуспех, чтобы при вызове необходимо
# было всегда явно проверять успех распаковки. update_field_values (и не только) вынести
# из класса сделать просто функцией.

# TODO измерить скорость, сравнить с cant, попробовать получить cython файлы в
# __pycache__ для ускорения

# TODO добавить модель подписки поверх того, что реализовано (совместимую с cant).
# Добавить возможность подписываться на многих отправителей, на типы сообщений.

# TODO добавить проверку на то, что не должны одновременно использоваться max/min
# ограничения и список допустимых значений


@dataclass
class ValueConstraints:
    bits_count: InitVar[int]
    min_value: Optional[int] = None
    max_value: Optional[int] = None
    _allowed_set: Optional[List[int]] = None
    allowed_list: InitVar[Optional[List[int]]] = None

    def __post_init__(self, bits_count: int, allowed_list: Optional[List[int]] = None):
        # TODO в юнит-тесты конфигурации протокола добавить проверки
        # - на то, что минимальные и максимальные допустимые значения лежат в пределах
        # 0..2**(bits_count - 1)
        # - то, что не используется одновременно allowed_list и min | max
        if self.max_value is None:
            self.max_value = 2 ** (bits_count - 1)
        if self.min_value is None:
            self.min_value = 0
        if allowed_list is not None:
            self._allowed_set = set(allowed_list)


@dataclass
class FieldProperties:
    name: str
    start: int  # начиная с 1 и до 16 или 32
    length: int
    shift_from_right: Optional[int] = None
    default_value: Optional[int] = None
    value_constraints: Optional[ValueConstraints] = None
    arbitration_bits_count: InitVar[int] = None

    def __post_init__(self, arbitration_bits_count: int):
        self.shift_from_right = arbitration_bits_count - (self.start + self.length - 1)
        if self.value_constraints is None:
            self.value_constraints = ValueConstraints(bits_count=self.length)
        else:
            self.value_constraints = ValueConstraints(
                bits_count=self.length, **self.value_constraints
            )

    def value_is_allowed(self, value: int):
        if self.value_constraints._allowed_set is not None:
            return value in self.value_constraints._allowed_set
        else:
            return (
                self.value_constraints.min_value
                <= value
                <= self.value_constraints.max_value
            )


@dataclass
class CANProtocol:
    protocol_file: InitVar[Union[str, os.PathLike]]
    arbitration_bits_count: int = 32  # для случая extended_id
    extended_id: bool = True
    fields: Dict[str, FieldProperties] = field(default_factory=dict)

    def __post_init__(self, protocol_file: Union[str, os.PathLike]):
        with open(protocol_file) as config_file:
            protocol_config = json.load(config_file)

        arbitration_config = protocol_config["arbitration_id"]
        self.extended_id = arbitration_config["extended"]
        self.arbitration_bits_count = 32 if self.extended_id else 16

        arbitration_fields = arbitration_config["fields"]

        for field_properties in arbitration_fields:
            self.fields.update(
                {
                    field_properties["name"]: FieldProperties(
                        arbitration_bits_count=self.arbitration_bits_count,
                        **field_properties,
                    )
                }
            )


@dataclass
class CANMessagesMediator:
    protocol_file: InitVar[Union[str, os.PathLike]]
    _arbitration_id: int = None
    _field_values: Dict[str, int] = field(default_factory=dict)
    _can_protocol: CANProtocol = None

    def __post_init__(self, protocol_file: Union[str, os.PathLike]):
        self._can_protocol = CANProtocol(protocol_file)
        for field_properties in self._can_protocol.fields.values():
            if field_properties.default_value is not None:
                self.field_values.update(
                    {field_properties.name: field_properties.default_value}
                )

    @property
    def arbitration_id(self):
        return self._arbitration_id

    def set_arbitration_id(self, arbitration_id: int) -> bool:
        """Установить arbitration_id и вычислить значения остальных полей.

        Parameters
        ----------
        arbitration_id : int

        Returns
        -------
        bool
            True если изменения прошло успешно. Иначе, например, при недопустимых
            значениях, False.
        """
        if arbitration_id > 2 ** (self._can_protocol.arbitration_bits_count - 1):
            return False
        else:
            # TODO проверкe при распаковке каждого поля
            for field_properties in self._can_protocol.fields.values():
                # маска из единиц, длиной равной равной длине поля
                bits_mask = (1 << field_properties.length) - 1
                # смещенная маска: в поле все единицы, вне поля слева и справа все нули
                mask_shifted = bits_mask << field_properties.shift_from_right
                # значение поля - пересечение с маской и сдвиг обратно вправо
                value = (
                    arbitration_id & mask_shifted
                ) >> field_properties.shift_from_right
                self._field_values.update({field_properties.name: value})

        return True

    # Для запрета назначения field_values напрямую. Необходимо использовать публичный
    # метод update_field_values, при вызове которого обновляется arbitration_id
    @property
    def field_values(self):
        return self._field_values

    def update_field_values(self, **kwargs) -> bool:
        """Обновить значения полей и вычислить arbitration_id.

        Returns
        -------
        bool
            True если изменения полей прошло успешно. Иначе, например, при недопустимых
            значениях, False.
        """
        for field_name, field_value in kwargs.items():
            if not self._can_protocol.fields[field_name].value_is_allowed(field_value):
                return False

        self._field_values.update(**kwargs)
        self._calc_arbitration_id()
        return True

    def _calc_arbitration_id(self):
        # FIXME возможно, для случая extended_id = False надо поставить другое значение
        self._arbitration_id = 1 << 31 if self._can_protocol.extended_id else 0
        for field_properties in self._can_protocol.fields.values():
            self._arbitration_id += (
                self.field_values[field_properties.name]
                << field_properties.shift_from_right
            )


# TODO проверочный код перенести в юнит-тесты и в документацию к модулю.
if __name__ == "__main__":
    can_mediator = CANMessagesMediator(PROTOCOL_FILENAME)

    if can_mediator.arbitration_id is not None:
        print("arbitration_id = ", can_mediator.arbitration_id)

    print(can_mediator.field_values)

    # Устанавливаем допустимые значения
    update_success = can_mediator.update_field_values(
        repeat=1,
        flags=1,
        id_CRC8=1,
        reciever_net=1,
        reciever_id=3,
        sender_net=1,
        sender_id=7,
        msg_type=1,
    )

    # Пробуем установить недопустимые значения. Объект не должен измениться.
    update_success = can_mediator.update_field_values(
        repeat=1,
        flags=1,
        id_CRC8=1,
        reciever_net=1,
        reciever_id=3,
        sender_net=1,
        sender_id=100,
        msg_type=1,
    )

    if not update_success:
        print("Not allowed values! Nothing changed.")
        print("Previous value:")

    arb_id = can_mediator.arbitration_id
    print("".join(map(str, range(10))).replace("0", "|") * 4)
    print(" " + bin(can_mediator.arbitration_id)[2:])

    update_success = can_mediator.set_arbitration_id(2**64 - 1)
    if not update_success:
        print("Not allowed arbitration_id! Nothing changed.")

    print(can_mediator.field_values)
