"""Bridge between 2 CAN buses."""

import asyncio
from typing import List

import can
from can.notifier import MessageRecipient

# TODO Возможно, необходимо добавить фильтр на допустимые ID для каждой шины, чтобы
# сообщение не копировалось бесконечно между двумя шинами. Т.е. если ID из списка
# оригинальных отправителей для данной шины, то это сообщение копируем в другую шину,
# иначе - это уже скопированное из другой шины сообщение и его копировать не
# нужно. Почему-то сейчас работает и без такого фильтра.


async def one_directional_bridge(input_bus: can.BusABC, output_bus: can.BusABC) -> None:
    """Infinitely copy messages from input CAN bus to output CAN bus.

    Parameters
    ----------
    input_bus : can.BusABC
        bus to copy from
    output_bus : can.BusABC
        bus to copy to
    """
    # https://python-can.readthedocs.io/en/stable/asyncio.html
    reader = can.AsyncBufferedReader()

    listeners: List[MessageRecipient] = [
        reader,  # AsyncBufferedReader() listener
    ]

    loop = asyncio.get_running_loop()
    can.Notifier(input_bus, listeners, loop=loop)

    while True:
        msg = await reader.get_message()
        output_bus.send(msg)


async def bi_directional_bridge(first_bus: can.BusABC, second_bus: can.BusABC) -> None:
    """Infinitely copy messages between two CAN buses.

    Parameters
    ----------
    first_bus : can.BusABC
        bus on one side of a bridge
    second_bus : can.BusABC
        bus on another side of a bridge
    """
    task_forward = asyncio.create_task(
        one_directional_bridge(input_bus=first_bus, output_bus=second_bus)
    )
    task_backward = asyncio.create_task(
        one_directional_bridge(input_bus=second_bus, output_bus=first_bus)
    )

    await task_forward
    await task_backward


if __name__ == "__main__":
    # В случае использования с физической шиной CAN
    # bus_physical = can.interface.Bus(
    #     bustype="socketcan", channel="vxcan0", bitrate=500000
    # )

    bus_physical = can.interface.Bus(
        "ws://mock_physical_can:54701/", bustype="remote", bitrate=500000
    )

    # https://github.com/christiansandberg/python-can-remote
    bus_ws = can.interface.Bus(
        "ws://can_server:54701/", bustype="remote", bitrate=500000
    )

    asyncio.run(bi_directional_bridge(first_bus=bus_physical, second_bus=bus_ws))
