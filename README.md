# Космические вычислительные системы

## Участники

- Жданова Кристина Александровна
- Жумаев Зайнулла Серикович
- Иванова Марина Александровна
- Каменев Никита Дмитриевич
- Щеглов Георгий Александрович

## Документация

Документация по проекту представлена в папке [docs](docs)

## Литература

- [Курс на Stepik](https://stepik.org/course/123960/syllabus)
- [Диссертация](https://mai.ru/upload/iblock/183/vpbzeo5ll25g7p8a6bq03ml3hm0sj3n7/Dissertatsiya_ZHumaev.pdf)

## Команды

<!-- docker-compose build --no-cache -->
./rebuild.sh - пересобрать контейнеры

./compose_run.sh - запустить контейнеры в docker-compose
