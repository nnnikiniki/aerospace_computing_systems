# This is PHOTO client for piCamera

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import socket
import time
from datetime import datetime

# переменные управления режимом
mode = 0
isRunning = True
isWaitForDateTime = False
cmdCodeTakePicture = 100  # коды команд
cmdCodeSetResolution = 101  # коды команд
cmdCodeSetParameters = 103  # коды команд
cmdCodeSetDataTime = 104  # коды команд
cmdCodeStopCommand = 105  # коды команд
cmdList = {
    cmdCodeTakePicture,
    cmdCodeSetResolution,
    cmdCodeSetParameters,
    cmdCodeSetDataTime,
    cmdCodeStopCommand,
}
cmdCode = cmdCodeTakePicture  # текущий полученный ID команды
cmdArgs = [0, 0, 0, 0, 0, 0, 0, 0]  # текущие полученные аргументы команды

iterator = 0

# переменные управления камерой
rawFormat = "rgb"
resolutionW = 2592
resolutionH = 1936
photosToMake = 1
photosTimeInterval_s = 1.0
isCmdReceived = True
camera_open = False
photosFolder = "/home/pi/Desktop/camera/1/"
photoCurrentNumber = (
    1  # при старте приложения нужно вначале найти последний по номеру файл в папке фото
)

balanceWhite = "auto"  # баланс белого
brightness = 50  # яркость, "0" - самый темный
exposure = "auto"  # экспозиция
excerpt = 0  # выдержка, мкс
iso = 0  # чувствительность к свету, доступные значения: 100,200,320,400,500,640,800.
# Чем выше ISO, тем лучше камера будет работать в условиях низкой освещенности, но тем
# более зернистым будет изображение или видео
contrast = 0  # контраст (-100..100)
saturation = 0  # насыщенность(-100..100)
sharpness = 0  # резкость(-100..100)

data_time = datetime.now()  # переменная полученных даты и времени

# реализация клиента
PORT = 3425
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("localhost", PORT))
s.settimeout(0.1)
data = []
answer_template = []
ok_template = []
err_template = []


def find_maxNumber_photo():
    global photoCurrentNumber

    max_number = 0
    for root, dirs, files in os.walk(photosFolder):
        for filename in files:
            # print(filename)
            name = filename.replace("R", "")
            # print (name)
            number = int(name, 16)
            if number > max_number:
                max_number = number
        print("Последнее фото в папке:", max_number)
        photoCurrentNumber = max_number + 1
    return 0


find_maxNumber_photo()


def command_run(cmdCode):
    global mode
    global photosToMake
    global cmdArgs
    global photosTimeInterval_s
    global brightness
    global excerpt
    global iso
    global contrast
    global saturation
    global sharpness
    global balanceWhite
    global exposure
    global data_time
    global isWaitForDateTime

    if cmdCode == cmdCodeTakePicture:
        x1 = [cmdArgs[0], cmdArgs[1], cmdArgs[2], cmdArgs[3]]
        photosToMake = int.from_bytes(x1, "little", signed=False)
        print("Кол-во фото:", photosToMake)

        x2 = [cmdArgs[4], cmdArgs[5], cmdArgs[6], cmdArgs[7]]
        photosTimeInterval_s = int.from_bytes(x2, "little", signed=False)
        print("Интервал между фото:", photosTimeInterval_s)

        # на каждую команду формироваться должно два ответа: один при получении команды,
        # второй после выполнения

        mode = 1
        return 0

    elif cmdCode == cmdCodeSetResolution:
        x3 = [cmdArgs[0], cmdArgs[1], cmdArgs[2], cmdArgs[3]]
        resolutionW = int.from_bytes(x3, "little", signed=False)
        print("Ширина, пиксели:", resolutionW)

        x4 = [cmdArgs[4], cmdArgs[5], cmdArgs[6], cmdArgs[7]]
        resolutionH = int.from_bytes(x4, "little", signed=False)
        print("Высота, пиксели:", resolutionH)

        command_evaluated_ok_respond()
        return 0

    elif cmdCode == cmdCodeSetParameters:
        if cmdArgs[0] == 1:
            balanceWhite = "auto"
        else:
            balanceWhite = "off"

        if cmdArgs[1] == 1:
            exposure = "auto"
        else:
            exposure = "off"

        brightness = int.from_bytes([cmdArgs[2]], "little", signed=False)
        print("Яркость:", brightness)

        excerpt = int.from_bytes([cmdArgs[3]], "little", signed=False)
        print("Выдержка", excerpt, "мкс")

        iso = 10 * (int.from_bytes([cmdArgs[4]], "little", signed=False))
        print("Чувствительность к свету", iso)

        contrast = int.from_bytes([cmdArgs[5]], "little", signed=True)
        print("Контраст:", contrast)

        saturation = int.from_bytes([cmdArgs[6]], "little", signed=True)
        print("Насыщенность:", saturation)

        sharpness = int.from_bytes([cmdArgs[7]], "little", signed=True)
        print("Резкость:", sharpness)

        command_evaluated_ok_respond()
        return 0

    elif cmdCode == cmdCodeSetDataTime:
        x5 = [cmdArgs[0], cmdArgs[1], cmdArgs[2], cmdArgs[3]]
        get_time = int.from_bytes(x5, "little", signed=False)
        print("Unix time", get_time)
        data_time = datetime.utcfromtimestamp(get_time)
        print("Установить дату и время", data_time)
        print("Системное время", datetime.now())

        x6 = [cmdArgs[4], cmdArgs[5]]
        photosToMake = int.from_bytes(x6, "little", signed=False)
        print("Кол-во фото:", photosToMake)

        x7 = [cmdArgs[6], cmdArgs[7]]
        photosTimeInterval_s = int.from_bytes(x7, "little", signed=False)
        print("Интервал между фото:", photosTimeInterval_s)

        isWaitForDateTime = True

        command_evaluated_ok_respond()
        return 0

    elif cmdCode == cmdCodeStopCommand:
        command_evaluated_ok_respond()
        mode = 3
        return 0

    return 0


def command_received_respond():
    global answer_template
    global data
    answer_template = bytes(
        [
            ord("C"),
            ord("A"),
            ord("N"),
            cmdCode,
            ord("R"),
            0,
            0,
            0,
            0,
            0,
            0,
            8,
            0,
            0,
            data[14],
            data[15],
            data[16],
            0,
            25,
            data[19],
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ]
    )
    s.sendall(answer_template)
    print("Cmd received")
    return 0


def command_evaluated_ok_respond():
    global ok_template
    global data
    ok_template = bytes(
        [
            ord("C"),
            ord("A"),
            ord("N"),
            cmdCode,
            ord("R"),
            ord("O"),
            0,
            0,
            0,
            0,
            0,
            8,
            0,
            0,
            data[14],
            data[15],
            data[16],
            0,
            25,
            data[19],
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ]
    )
    s.sendall(ok_template)
    print("Cmd evaluated OK")
    return 0


def command_evaluated_error_respond():
    global err_template
    global data
    err_template = bytes(
        [
            ord("C"),
            ord("A"),
            ord("N"),
            cmdCode,
            ord("R"),
            ord("E"),
            ord("O"),
            ord("U"),
            ord("T"),
            0,
            0,
            8,
            0,
            0,
            data[14],
            data[15],
            data[16],
            0,
            25,
            data[19],
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ]
    )
    s.sendall(err_template)
    print("Cmd evaluated error")
    return 0


while isRunning:
    try:
        data = s.recv(41 * 4, socket.MSG_DONTWAIT)
        print("data")
        DAT = []

        for k in range(41):
            AA = data[k * 4 : k * 4 + 3]
            int_data = int.from_bytes(AA, "little", signed=False)
            DAT.append(int_data)
            print(k, " ", int_data, " ", DAT[-1])
        print("end data")
        print(DAT)
        print("end data")
        if (
            (data[0] == ord("C")) and (data[1] == ord("A")) and (data[2] == ord("N"))
        ):  # пришел кадр CAN
            if data[19] == 0:  # тип кадра - команда (Type=0)
                cmdCode = data[14]  # ID_CRC_8 код команды
                i = 1
                for i in range(8):
                    cmdArgs[i] = data[3 + i]  # data frame 8 Байт
                    i += 1
                isCmdReceived = True
                if (isCmdReceived is True) and (cmdCode in cmdList):
                    isCmdReceived = False
                    command_received_respond()
                    print("Аргументы команды", cmdArgs)
                    command_run(cmdCode)
                    print("Received data", data)
                else:
                    time.sleep(1)
                    if isCmdReceived is True:
                        isCmdReceived = False
    except:
        pass

    if mode == 0:  # слушаем TCP-IP на предмет команд
        if isWaitForDateTime:
            if data_time <= datetime.now():
                mode = 1

    elif mode == 1:  # готовим камеру к фотографированию
        try:
            # флаг для проверки того, что камера не занята (не открыта) для другой
            # команды
            if camera_open is False:
                time.sleep(2)

                camera_open = True

                # time1 = datetime.now()
                # print ('Time1 %d' % time1.second)

            mode = 2
            iterator = 0
        except:
            mode = 0
            isWaitForDateTime = False

    elif mode == 2:  # делаем следующее фото, пока не сделаем столько, сколько попросили
        if photoCurrentNumber <= 4095:
            if iterator < photosToMake:
                # camera.capture(
                #     photosFolder + "R" + ("%0.3X" % photoCurrentNumber), rawFormat
                # )
                photoCurrentNumber += 1
                time.sleep(photosTimeInterval_s)
                iterator += 1
            else:
                mode = 3
        else:
            command_evaluated_error_respond()

    elif mode == 3:  # возвращаем камеру в исходное состояние
        # time2 = datetime.now()
        # print ('Time2 %d' % time2.second)
        try:
            # camera.close()
            camera_open = False
        except:
            pass

        command_evaluated_ok_respond()
        mode = 0
        isWaitForDateTime = False
