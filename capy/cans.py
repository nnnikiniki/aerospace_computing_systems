# объект для работы с CAN-сокетом и подписками.
# он аналогичен cans в С++ по функционалу но не обертывает его функций
import can
from cantpy import cant


######################
class cans(object):
    cant_frame = cant()

    def st(self, dev_name):
        print("dev_name=", dev_name)

    def start(self, dev_name: str):
        cans.bus = can.Bus(interface="socketcan", channel=dev_name, bitrate=250000)

    def stop(self):
        cans.bus.shutdown()

    def sendFrameIdData(self, Id, Data: str):
        print("Data in sendFrameIdData = ", Data)
        if len(Data) > 8:
            Data = Data[:8]
        if len(Data) < 8:
            # print("len(data)=", len(Data))
            for j in range(len(Data), 8):
                Data += chr(0)
            # print("now data=", Data)

        msg = can.Message(
            arbitration_id=Id,
            data=bytes(Data, "ascii"),
            is_extended_id=True,
        )
        try:
            cans.bus.send(msg)
            # print(f"Message sent on {bus.channel_info}")
        except can.CanError:
            print("cans error: Message NOT send!")

    def sendFrame(self, frame: cant):
        msg = can.Message(
            arbitration_id=frame.getAdr(),
            data=frame.Data,
            is_extended_id=True,
        )
        try:
            cans.bus.send(msg)
            # print(f"Message sent on {bus.channel_info}")
        except can.CanError:
            print("cans error: Message NOT send!")

    def initSub(self, lines):
        print("LINES")
        print(lines)
        print("Details")

        cans.name = lines[0][:-1]  # device name like "can0"
        cans.regime = int(lines[1][:-1])  # working regime

        # подписка по ключам
        cans.key_count = int(lines[2][:-1])  # number of key field in canid file
        cans.SubS_count = int(lines[3][:-1])  # number of SubS lines

        cans.SubS = []
        for line in lines[4:]:
            cans.SubS.append(list(map(int, line.split())))

        print("S=", cans.SubS)
        print("LINES OK")

    def loadSub(self, SubFileName: str):
        with open(SubFileName) as sub_file:
            lines = sub_file.readlines()
        self.initSub(lines)

    def printSub(self, ndx: int):
        print(
            "subscribe No",
            ndx,
            " : Rx",
            cans.SubS[ndx][:4],
            " Tx",
            cans.SubS[ndx][4:],
        )

    def printSubs(self):
        print("Subscribe list")
        for i in range(cans.SubS_count):
            self.printSub(i)

    def sendFrameSub(self, frame: cant, sub: int, print_frame: bool):
        # Data = "12345678"
        # for i in range(frame.getIdAttrSize()):
        #    print("attr i=", i, " key=", frame.getIdAttrKeyByNdx(i))

        c = cans.key_count

        for i in range(frame.getIdAttrSize()):
            # print("c=", c, " i=", i)
            # print(" key=", frame.getIdAttrKeyByNdx(i))

            if frame.getIdAttrKeyByNdx(i):
                #    print(" ! c=", c, " SubS[sub][c]=", cans.SubS[sub][c])
                frame.setAttrByNdx(i, cans.SubS[sub][c])
                c += 1

        frame.codeId()

        if print_frame:
            frame.print_frame()

        msg = can.Message(
            arbitration_id=frame.getAdr(),
            data=frame.Data,
            is_extended_id=True,
        )
        try:
            cans.bus.send(msg)
            # print(f"Message sent on {bus.channel_info}")
        except can.CanError:
            print("cans error: Message NOT send!")

    def rcvFrameSub(self, frame: cant) -> int:
        for k in range(self.SubS_count):
            Q = True
            c = 0
            for i in range(frame.getIdAttrSize()):
                # print("i=", i)
                if frame.getIdAttrKeyByNdx(i):
                    # print("c=", c, "Q=", Q)
                    # print("k=", k, "SubS=", self.SubS[k][c])
                    Q = Q and (frame.getAttrByNdx(i) == self.SubS[k][c])
                    # print("Q=", Q)
                    if not Q:
                        break
                    c += 1

            if Q:
                # print("!!! k=", k)
                return k

        return -1

    # int recvFrame(cant *F, int mode, int timeout_sec);
