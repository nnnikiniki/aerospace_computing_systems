#ifndef ZPY_H
#define ZPY_H

#include "cant.h"
#include <stdio.h>

//*
extern "C"
{
    //    // include below each method you want to make visible outside
    cant *init() { return new cant(); }

    void InitId(cant *self){printf("iID \n"); self->InitId(); printf("iID2 \n");}

    void ClearIdStructure(cant *self){self->ClearIdStructure();}
    void AddIdStruct(cant *self,const char  Name[], uint8_t Length, bool Key){self->AddIdStruct(Name, Length, Key);}

    uint8_t getIdAttrSize(cant *self) { return self->getIdAttrSize(); }
    bool getLoadId(cant *self){return self->getLoadId();}

    void codeId(cant *self) { self->codeId(); }
    void decodeId(cant *self) { self->decodeId(); }
    bool setAttrByNdx(cant *self, int ndx, uint8_t Val){return self->setAttrByNdx(ndx, Val);}
    bool setAttrByName(cant *self, const char  Name[], uint8_t Val){return self->setAttrByName(Name, Val);}
    uint32_t getAttrByNdx(cant *self, int ndx){return self->getAttrByNdx(ndx);}
    uint32_t getAttrByName(cant *self,const char  Name[]){return self->getAttrByName(Name);}
    uint32_t getIdAttrMaxValByNdx(cant *self, int ndx){return self->getIdAttrMaxValByNdx(ndx);}

    uint32_t getIdAttrKeyByNdx(cant *self, int ndx){return self->getIdAttrKeyByNdx(ndx);}



    uint32_t getAdr(cant *self){return self->getAdr();}
    void setAdr(cant *self, uint32_t adr){self->setAdr(adr);}


    void test(cant *self, const char a[]){
           //printf("\n a= %s \n",a);
    self->test(a);
    }//void test

}
//*/

/*
extern "C"
{
//    // include below each method you want to make visible outside
    Test* init(int k) {return new Test(k);}
    void setInt(Test *self, int k) {self->setInt(k);}
    int getInt(Test *self) {return self->getInt();}
}
//*/

#endif
