# испытание (то есть тестирование, но не автоматическое) методов класса cans
#
from cans import cans

from cant import cant

cans_obj = cans()
frame = cant()

print("loadSub")
cans_obj.loadSub("canconftst")

print("cans.name=", cans_obj.name)
print("cans.regime=", cans_obj.regime)
print("cans.KeyCount=", cans_obj.key_count)
print("cans.SubScount=", cans_obj.SubS_count)
print("loadSub OK")

cans_obj.printSubs()

# quit()
print("start")

cans_obj.start("vcan0")
print("sendFrame")
cans_obj.sendFrameIdData(0, "1111")

print("frame erlernen")
print("LoadId=", frame.getLoadId())
print("IdAttrSize=", frame.getIdAttrSize())

for i in range(frame.getIdAttrSize()):
    frame.setAttrByNdx(i, 1)


frame.codeId()
frame.setData("abcdefghij")
frame.print_frame()

cans_obj.sendFrame(frame)
cans_obj.sendFrameSub(frame, 1, True)
cans_obj.sendFrameSub(frame, 0, True)

print("\n Recieve 1")
frame.setAttrByNdx(3, 0)
frame.setAttrByNdx(4, 12)
frame.setAttrByNdx(5, 0)
frame.setAttrByNdx(6, 11)
frame.print_frame()
rez = cans_obj.rcvFrameSub(frame)
print("rezultat=", rez)
print("\n Recieve2")
frame.setAttrByNdx(3, 0)
frame.setAttrByNdx(4, 13)
frame.setAttrByNdx(5, 0)
frame.setAttrByNdx(6, 11)
frame.print_frame()
rez = cans_obj.rcvFrameSub(frame)
print("rezultat=", rez)
print("\n Recieve3")
frame.setAttrByNdx(3, 0)
frame.setAttrByNdx(4, 12)
frame.setAttrByNdx(5, 0)
frame.setAttrByNdx(6, 25)
frame.print_frame()
rez = cans_obj.rcvFrameSub(frame)
print("rezultat=", rez)
print("\n Recieve4")
frame.setAttrByNdx(3, 0)
frame.setAttrByNdx(4, 12)
frame.setAttrByNdx(5, 0)
frame.setAttrByNdx(6, 26)
frame.print_frame()
rez = cans_obj.rcvFrameSub(frame)
print("rezultat=", rez)

print("stop")
cans_obj.stop()


print("object cans ist gluecklich erlernt!!!")
