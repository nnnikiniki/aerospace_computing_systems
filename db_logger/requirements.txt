numpy==1.23.2
python-can==4.1.0
python-can-remote==0.2.1
SQLAlchemy==2.0.8
psycopg2-binary==2.9.5
