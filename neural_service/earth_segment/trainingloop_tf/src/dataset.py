import pandas as pd
import tensorflow as tf


def labels_prepare(config):
    labels_df = pd.read_csv(config.labels_dir)

    label_list = []
    for tag_str in labels_df["tags"].values:
        labels = tag_str.split(" ")
        for label in labels:
            if label not in label_list:
                label_list.append(label)

    n_classes = len(label_list)

    for label in label_list:
        labels_df[label] = labels_df["tags"].apply(
            lambda x: 1 if label in x.split(" ") else 0
        )

    labels_df["image_name"] = "train/" + labels_df["image_name"] + ".jpg"

    return labels_df, n_classes


def get_dataloader(config):
    labels_df, n_classes = labels_prepare(config)

    VAL_SPLIT = 0.2
    TARGET_SIZE = (config.img_size, config.img_size)

    datagen = tf.keras.preprocessing.image.ImageDataGenerator(
        rescale=1.0 / 255, validation_split=VAL_SPLIT
    )

    training_generator = datagen.flow_from_dataframe(
        labels_df,
        directory=config.img_dir,
        x_col="image_name",
        y_col=list(labels_df.columns[2:]),
        target_size=TARGET_SIZE,
        class_mode="raw",
        batch_size=config.batch_size,
        seed=32,
        subset="training",
        validate_filenames=False,
    )

    validation_generator = datagen.flow_from_dataframe(
        labels_df,
        directory=config.img_dir,
        x_col="image_name",
        y_col=list(labels_df.columns[2:]),
        target_size=TARGET_SIZE,
        class_mode="raw",
        batch_size=config.batch_size,
        seed=32,
        subset="validation",
        validate_filenames=False,
    )

    return training_generator, validation_generator, n_classes


# VAL_SPLIT = 0.2
# BATCH_SIZE = 64
# TARGET_SIZE = (128, 128)

# datagen = tf.keras.preprocessing.image.ImageDataGenerator(
#     rescale=1.0/255,
#     validation_split=VAL_SPLIT)

# training_generator = datagen.flow_from_dataframe(
#     labels_df,
#     directory=PLANET_KAGGLE_JPEG_DIR,
#     x_col='image_name',
#     y_col=list(labels_df.columns[2:]),
#     target_size=TARGET_SIZE,
#     class_mode='raw',  # categorical / multi_output / raw
#     batch_size=BATCH_SIZE,
#     seed=0,
#     subset='training')

# validation_generator = datagen.flow_from_dataframe(
#     labels_df,
#     directory=PLANET_KAGGLE_JPEG_DIR,
#     x_col='image_name',
#     y_col=list(labels_df.columns[2:]),
#     target_size=TARGET_SIZE,
#     class_mode='raw',
#     batch_size=BATCH_SIZE,
#     seed=0,
#     subset='validation')
