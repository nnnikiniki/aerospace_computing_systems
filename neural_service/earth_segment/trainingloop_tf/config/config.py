from src.base_config import Config

SEED = 42
IMG_SIZE = 224
CHANNEL = 3
BATCH_SIZE = 8
N_CLASSES = 17
N_EPOCHS = 30
NUM_ITERATION_ON_EPOCH = 100

IMAGE_DIR = "D:/dataset/raw/"
LABELS_PATH = "D:/dataset/raw/labels.csv"

DEVICE = "cuda"
PATH_MODEL = "./weights/"

AUGMENT = True
MODEL_NAME = "MobileNetv2"

THRESHOLD = 0.1
LOGGERS_NAME = "Amazon_from_Space"
IMAGE_TEST = "tests/fixtures/images/file_22.jpg"
TEST_DIR = "./tests/"
CLASSES = [
    "haze",
    "primary",
    "agriculture",
    "clear",
    "water",
    "habitation",
    "road",
    "cultivation",
    "slash_burn",
    "cloudy",
    "partly_cloudy",
    "conventional_mine",
    "bare_ground",
    "artisinal_mine",
    "blooming",
    "selective_logging",
    "blow_down",
]

SIZE_VAL = 0.2
SIZE_TEST = 0.05
STEP_OPT = 7
GAMMA = 0.1
LR = 1e-4


config = Config(
    seed=SEED,
    step_size=STEP_OPT,
    gamma=GAMMA,
    lr=LR,
    img_size=IMG_SIZE,
    channel=CHANNEL,
    batch_size=BATCH_SIZE,
    n_epoch=N_EPOCHS,
    img_dir=IMAGE_DIR,
    labels_dir=LABELS_PATH,
    device=DEVICE,
    path_models=PATH_MODEL,
    augmentation=AUGMENT,
    model_name=MODEL_NAME,
    img_test=IMAGE_TEST,
    test_dir=TEST_DIR,
    # loggers
    project_name=LOGGERS_NAME,
    size_val=SIZE_VAL,
    size_test=SIZE_TEST,
    threshold=THRESHOLD,
    classes=CLASSES,
)
