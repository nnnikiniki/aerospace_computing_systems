import argparse
import difflib

from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument(
    "--old",
    help="path to the old model",
    default=None,
)

parser.add_argument(
    "--new",
    help="path to the new model",
    default=None,
)

parser.add_argument(
    "--out",
    help="path to the out model",
    default=None,
)
args = parser.parse_args()


def apply_binary_patch(old_file_path, patch_file_path, output_file_path):
    with open(old_file_path, "rb") as old_file:
        old_data = old_file.read()

    with open(patch_file_path, "rb") as patch_file:
        patch_data = patch_file.read()

    d = difflib.Differ()
    diff = d.compare(old_data, patch_data)
    print("diff compare")
    new_data = bytearray(old_data)
    offset = 0
    print(diff)
    for line in tqdm(diff):
        code = line[0]
        if code == " ":
            offset += len(line[2:])
        elif code == "+":
            data = bytearray(line[2:].encode())
            new_data[offset:offset] = data
            offset += len(data)

    with open(output_file_path, "wb") as output_file:
        output_file.write(new_data)


def binary_diff(file1_path, file2_path, output_file_path):
    with open(file1_path, "rb") as file1:
        file1_data = file1.read()
    # for cod in file1_data:
    #     print(cod.)
    # break

    with open(file2_path, "rb") as file2:
        file2_data = file2.read()

    d = difflib.Differ()
    diff = d.compare(file1_data, file2_data)
    print(diff)
    for dif in diff:
        print(dif.encode())
    diff_data = b"".join(line[2:].encode() for line in diff if line[0] == "+")

    with open(output_file_path, "wb") as output_file:
        output_file.write(diff_data)


binary_diff(args.old, args.new, args.out)
