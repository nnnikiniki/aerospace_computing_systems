import os

from loguru import logger

from utils.convert_tflite import convert_model_tf
from utils.swap_diff_tf import apply_param_diff_h5

if __name__ == "__main__":
    list_name = ["weights/newbestmodel.h5", "weights/param_diff.h5"]

    apply_param_diff_h5("weights/bestmodel.h5", list_name[1], list_name[0])

    logger.debug("Swap diff model")

    convert_model_tf(list_name[0])

    logger.debug("new model successfully converted to tflite")

    [os.remove(f) for f in list_name if os.path.exists(f)]

    logger.debug("unnecessary files removed")
