import argparse
import os

from loguru import logger
from omegaconf import OmegaConf
from PIL import Image
from src.classification_planet import PlanetClassifier

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
path_model = os.path.join(BASE_DIR, "weights/bestmodel.tflite")

cfg = OmegaConf.load("config/config.yml")

parser = argparse.ArgumentParser()
parser.add_argument(
    "-i",
    "--image",
    help="path to the input image",
    default=os.path.join(BASE_DIR, "tests", "fixtures", "images", "file_22.jpg"),
)
args = parser.parse_args()


def predict(image_path):
    image = Image.open(image_path)
    model = PlanetClassifier(cfg)

    logger.debug(model.predict_proba(image))

    predict = model.predict_proba(image)
    logger.debug(predict)
    response = {"predict": predict}

    return response


if __name__ == "__main__":
    pred = predict(args.image)

    print(pred)
    with open("data/result.txt", "w") as file:
        file.write(pred["predict"])
