import os

current_dir = os.getcwd()
file_path = os.path.join(current_dir, "tutorial/bestmodel.tflite")
print(file_path)
command = (
    f"docker run -v {file_path}:/planet_service/weights/bestmodel.tflite planet_service"
)
os.system(command)
