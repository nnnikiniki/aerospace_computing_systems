#ifndef CANT_H
#define CANT_H

#include "stdint.h"
#include <algorithm>
#include <linux/can.h>
#include <string>
#include <vector>

class cant
{
  private:
    static const uint32_t CanEFFflg
        = CAN_EFF_FLAG; // 0x80000000U; // EFF/SFF is set in the MSB //
    static const uint8_t IdSize = 29;

    // 29 bit CAN adress
    // 10011111 00000000 00000001 00000001
    // 87654321 87654321 87654321 87654321
    // E  Flags Attrib   Reciver  Sender
    // F  0..31 0...256  0...256  0...256
    // F
    // byte[3]  byte[2]  byte[1]  byte[0]

    bool err;
    bool LoadId;


  public:
    std::vector<std::string> IdAttrName;//field names
    std::vector<uint8_t> IdAttrLength;//field length in bit
    std::vector<bool> IdAttrKey;//key field (1) or not (0) for identification
    std::vector<uint32_t> IdAttrValue;
    std::vector<uint32_t> IdAttrMask;
    std::vector<uint8_t> IdAttrShift;
    std::vector<uint32_t> IdAttrMaxVal;

    static const uint8_t dlc = 8; // data length in bytes

    can_frame frm;

    cant(std::vector<std::string> name
         = { "Flags", "Attribute", "Receiver", "Sender" },
         std::vector<uint8_t> length = { 5, 8, 8, 8 }, std::vector<bool> key = { 0, 0, 1, 0 });

    void InitId();
    void ClearIdStructure();
    void AddIdStruct(std::string Name, uint8_t Length, bool Key);

    uint8_t getIdAttrSize();
    bool getLoadId();

    void codeId();
    void decodeId();

    bool setAttrByName(std::string Name, uint8_t Val);
    bool setAttrByNdx(int ndx, uint8_t Val);
    uint32_t getAttrByName(std::string Name);
    uint32_t getAttrByNdx(int ndx);
    uint32_t getIdAttrMaxValByNdx(int ndx);

    uint8_t getIdAttrKeyByNdx(int ndx);


    uint32_t getAdr();
    void setAdr(uint32_t adr);

    uint8_t getKeyCount();

    can_frame *getFrame(); // get can_frame

    bool input8char(char *C); // char[8]->data

    bool inputInt32(uint32_t Val, uint8_t StartPos);//32 bit (4 byte) -> data[StartPos]

    uint32_t outputInt32(uint8_t StartPos);// data[StartPos] -> 32 bit (4 byte)

    uint8_t PrintFrame();
    void PrintIdStructure();

    void test(std::string a);

    ~cant();

}; // class cant

#endif // CANT_H
