class cant(object):
    Data = b""

    CanEFFflg = int(0x80000000)
    #     = CAN_EFF_FLAG; // 0x80000000U; // EFF/SFF is set in the MSB //
    IdSize = 29

    CAN_id = 0

    # // 29 bit CAN adress
    # // 10011111 00000000 00000001 00000001
    # // 87654321 87654321 87654321 87654321
    # // E  Flags Attrib   Reciver  Sender
    # // F  0..31 0...256  0...256  0...256
    # // F
    # // byte[3]  byte[2]  byte[1]  byte[0]
    CAN_ID_FILE_NAME = "cantid"

    err = False
    LoadId = False

    IdAttrName = []
    IdAttrLength = []
    IdAttrKey = []
    IdAttrValue = []
    IdAttrMask = []
    IdAttrShift = []
    IdAttrMaxVal = []

    dlc = 8

    def __init__(self, FN="cantid"):
        print("\n \n -------Start cant constructor!")

        global CAN_ID_FILE_NAME
        CAN_ID_FILE_NAME = FN
        print("--Stucture file name=", CAN_ID_FILE_NAME)

        global IdAttrName
        IdAttrName = ["Flags", "Attribute", "Receiver", "Sender"]
        global IdAttrLength
        IdAttrLength = [5, 8, 8, 8]
        global IdAttrKey
        IdAttrKey = [0, 0, 1, 0]

        self.SetId(IdAttrName, IdAttrLength, IdAttrKey)
        print("--Stucture Set N=", self.getIdAttrSize() )
        self.PrintIdStructure()

        global LoadId
        LoadId = False
        print("--Stucture Set NN=", self.getIdAttrSize() )
        self.LoadIdFile(self.CAN_ID_FILE_NAME)
        print("--Stucture Set NNN=", self.getIdAttrSize() )
        self.PrintIdStructure()
        print("\n  --cant constructor Ok!")

    # def __init__(self):

    def InitId(self):
        print("\n Start InitId!")

        # print("InitId#1\n")
        # building structure

        global IdAttrValue
        global IdAttrMask
        global IdAttrShift
        global IdAttrMaxVal

        IdAttrValue = []
        IdAttrMask = []
        IdAttrShift = []
        IdAttrMaxVal = []

        s = 0
        Uno = 1
        N = len(IdAttrName)
        # print("N =", N)

        for i in range(N):
            s += IdAttrLength[i]

        if s > self.IdSize:
            print(
                "CANT ERROR:(s=", s, ") > (IdSize=", self.IdSize, ") ; Fields count=", N
            )
            self.PrintIdStructure()
            exit(101)
        # print("s =", s)
        # init Values
        # print("\n InitId#2 \n")

        for i in range(N):
            IdAttrValue.append(0)
            IdAttrShift.append(0)
            IdAttrMask.append(Uno)
            s = 2
            for j in range(1, IdAttrLength[i]):
                s *= 2

            # print("MaxVal s=", s)
            IdAttrMaxVal.append(s)

        # building mask
        # print("\n InitId#3\n")
        # print("UNO=", bin(Uno))

        for i in range(N):
            # print(i, " IdAttrLength[i]=", IdAttrLength[i])
            for k in range(IdAttrLength[i] - 1):
                IdAttrMask[i] = IdAttrMask[i] << 1
                IdAttrMask[i] |= Uno
                # print("k=", k, " IdAttrMask[i]", bin(IdAttrMask[i]))

            # print("\n ", i, " IdAttrMask[i] ", bin(IdAttrMask[i]))
            for j in range((i + 1), N):
                for q in range(IdAttrLength[j]):
                    IdAttrShift[i] += 1

            IdAttrMask[i] = IdAttrMask[i] << IdAttrShift[i]
            IdAttrMask[i]
            # print(
            #     "finish: ",
            #     i,
            #     " IdAttrMask[i] ",
            #     f"{IdAttrMask[i]:036_b}".replace("_", " "),
            # )

        self.codeId()

    # print("InitId#4 \n InitId OK!!!!\n")

    # def InitId()

    def ClearIdStructure(self):
        # lib.ClearIdStructure(self.obj)
        print("\n ---- Start ClearIdStructure!")
        global IdAttrName
        global IdAttrLength
        global IdAttrKey

        IdAttrName = []
        IdAttrLength = []
        IdAttrKey = []

    # def ClearIdStructure(self):

    def AddIdStruct(self, Name, Length, Key):
        # lib.AddIdStruct(self.obj, Name, Length, Key)
        #print("AddIdStruct !")
        global IdAttrName
        global IdAttrLength
        global IdAttrKey
        IdAttrName.append(Name)
        IdAttrLength.append(Length)
        IdAttrKey.append(Key)
        # print("NNN=", len(IdAttrName))

    # def AddIdStruct(self, Name, Length, Key):

    def getIdAttrSize(self):
        return len(IdAttrName)

    def getLoadId(self):
        return LoadId

    def codeId(self):
        # print("\n ---Start codeId!")
        N = len(IdAttrName)
        # print("N=", N)
        Id = 0  # uint32_t

        for i in range(N):
            # print(
            #     "i=",
            #     i,
            #     " IdAttrValue[i]=",
            #     IdAttrValue[i],
            #     " IdAttrShift[i]=",
            #     IdAttrShift[i],
            # )
            S = IdAttrValue[i] << IdAttrShift[i]
            Id |= S
            # print("S=", f"{S:036_b}".replace("_", " "))
            # print("Id=", f"{Id:036_b}".replace("_", " "))

        self.CAN_id = Id | self.CanEFFflg
        # print("CanEFFflg=", f"{self.CanEFFflg:036_b}".replace("_", " "))
        # print("CAN_id=", f"{self.CAN_id:036_b}".replace("_", " "))

        # print("codeId Ok !")

    # def codeId(self):

    def decodeId(self):
        print("\n ----Start decodeId!")
        # print("CAN_id=", f"{self.CAN_id:036_b}".replace("_", " "))
        N = self.getIdAttrSize()

        Id = self.CAN_id
        for i in range(N):
            S = Id & IdAttrMask[i]  # uint32_t
            IdAttrValue[i] = S >> IdAttrShift[i]
        # self.print_frame()
        # print("\n decodeId Ok!")

    # def decodeId(self):

    def setAttrByNdx(self, ndx, Val):
        if (ndx >= 0) and (ndx < self.getIdAttrSize()):
            if Val > IdAttrMaxVal[ndx]:
                IdAttrValue[ndx] = 0
                return 0
            else:
                IdAttrValue[ndx] = Val
                return 1
        else:
            return 0

    # def setAttrByNdx(self, ndx, Val):

    def setAttrByName(self, Name, Val):
        if Name in IdAttrName:
            ndx = IdAttrName.index(Name)
            if Val > IdAttrMaxVal[ndx]:
                IdAttrValue[ndx] = 0
                return 0
            else:
                IdAttrValue[ndx] = Val
                return 1
        else:
            print("WRONG IdAttrName!!!")
            exit(102)
            return 0

    # def setAttrByName(self, Name, Val):

    def getAttrByNdx(self, ndx):
        if (ndx >= 0) and (ndx < self.getIdAttrSize()):
            return IdAttrValue[ndx]
        else:
            print("getIdAttrValueByNdx WRONG Ndx!!!")
            exit(103)
            return 0

    # def getAttrByNdx(self, ndx):

    def getAttrByName(self, Name):
        if Name in IdAttrName:
            ndx = IdAttrName.index(Name)
            return IdAttrValue[ndx]
        else:
            print("WRONG IdAttrName!!!")
            exit(102)
            return 0

    # def getAttrByName(self, Name):

    def getIdAttrMaxValByNdx(self, ndx):
        if (ndx >= 0) and (ndx < self.getIdAttrSize()):
            return IdAttrMaxVal[ndx]
        else:
            print("getIdAttrMaxValByNdx WRONG Ndx!!!")
            exit(103)
            return 0

    # def getIdAttrMaxValByNdx(self, ndx):

    def getIdAttrKeyByNdx(self, ndx):
        if (ndx >= 0) and (ndx < self.getIdAttrSize()):
            return IdAttrKey[ndx]
        else:
            print("getIdAttrKeyByNdx WRONG Ndx!!!")
            exit(103)
            return 0

    # def getIdAttrKeyByNdx(self, ndx):

    def getAdr(self):
        # print("Start getAdr")
        return self.CAN_id | self.CanEFFflg

    def setAdr(self, adr):
        # print("\n ----Start setAdr")
        self.CAN_id = adr

    def test(self, s):
        print("tets")

    def SetId(self, NM, LN, KY):
        print("\n ----Start SetId")
        print("SetId = len(NM)", len(NM))
        if len(NM) == len(LN) == len(KY):
            self.ClearIdStructure()
            # print("getIdAttrSize=", self.getIdAttrSize())

            for i in range(len(NM)):
                print(NM[i], " ", LN[i], " ", KY[i])
                self.AddIdStruct(bytes(NM[i], "ascii"), int(LN[i]), int(KY[i]))
            print("InitID")

            self.InitId()
            # print("getIdAttrSize=", self.getIdAttrSize())

            for i in range(0, self.getIdAttrSize()):
                print("i=", i, " maxVal=", self.getIdAttrMaxValByNdx(i))
        else:
            print("CANT ERROR!!! len(NM)!=len(LN)")
        print("SetId Ok")

    # def SetId(self, NM, LN, KY):

    def setData(self, new_data):
        d = str(new_data)
        # print("Set data=", d)
        if len(d) > 8:
            d = d[:8]
        if len(d) < 8:
            # print("len(data)=", len(d))
            for j in range(len(d), 8):
                d += chr(0)
            # print("now data=", d)

        cant.Data = bytes(d, "ascii")

    # def setData(self, new_data):

    def print_frame(self):
        id = []
        for i in range(self.getIdAttrSize()):
            id.append(self.getAttrByNdx(i))
        print("frm id=", id, " data=", self.Data)

    # def print_frame(self):

    def LoadIdFile(self, name):

        try:
            with open(name) as protocol_file:
                lines = protocol_file.readlines()

            global LoadId
            LoadId = True

            print("cant.py Load LINES")
            print(lines)
            print("load LINES OK")

            field_names1, field_lengths1, field_keys1 = zip(
                *(line.split() for line in lines)
            )

            print(field_names1, field_lengths1, field_keys1)
            print("SetId")
            self.SetId(field_names1, field_lengths1, field_keys1)
            print("SetId OK")
        except FileNotFoundError:
            print(f"Запрашиваемый файл { name } не найден")

    # def LoadIdFile(self, name):

    def PrintIdStructure(self):
        
        N = len(IdAttrName)
        #print("Print Id structure N=",len(IdAttrName))
        #print("Print Id structure IdAttrLength=",len(IdAttrLength))
        #print("Print Id structure IdAttrKey N=",len(IdAttrKey))
        #print("Print Id structure IdAttrMaxVal N=",len(IdAttrMaxVal))
        for i in range(N):
            print("Field#",i," Name=",IdAttrName[i]," Len=",IdAttrLength[i]," Key=",IdAttrKey[i],"  MaxVal=",IdAttrMaxVal[i])

    # def PrintIdStructure():

    def print_attr_list(self):
        pass
