import os
from datetime import datetime

from sqlalchemy import Column, DateTime, Integer, create_engine
from sqlalchemy.orm import declarative_base

print("Start database!")
# print("file=", __file__)
# print("CWD=", os.getcwd())

DIRNAMEDB = os.path.dirname(os.path.abspath(__file__))
print("DIR=", DIRNAMEDB)


SQLALCHEMY_DATABASE_URL = "sqlite:///" + DIRNAMEDB + "/sql_app.db"
# SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"

print("SQLALCHEMY_DATABASE_URL=", SQLALCHEMY_DATABASE_URL)


# создание движка
engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)


Base = declarative_base()

# exit(123)


class CANframe(Base):
    __tablename__ = "frames"
    id = Column(Integer, primary_key=True, index=True)
    tim = Column(DateTime, default=datetime.utcnow)
    adr = Column(Integer, default=0)
    d0 = Column(Integer, default=0)
    d1 = Column(Integer, default=0)
    d2 = Column(Integer, default=0)
    d3 = Column(Integer, default=0)
    d4 = Column(Integer, default=0)
    d5 = Column(Integer, default=0)
    d6 = Column(Integer, default=0)
    d7 = Column(Integer, default=0)
    # tim = Column(Float)
    # поля для расшифровки адреса кадра
    a0 = Column(Integer, default=0)
    a1 = Column(Integer, default=0)
    a2 = Column(Integer, default=0)
    a3 = Column(Integer, default=0)
    a4 = Column(Integer, default=0)
    a5 = Column(Integer, default=0)
    a6 = Column(Integer, default=0)
    a7 = Column(Integer, default=0)
    a8 = Column(Integer, default=0)
    a9 = Column(Integer, default=0)
    a10 = Column(Integer, default=0)


# создаем таблицы
# Base.metadata.create_all(bind=engine)


# создаем сессию подключения к бд
# SessionLocal = sessionmaker(autoflush=False, bind=engine)


# db = SessionLocal()

# # создаем объект Person для добавления в бд
# tframe = CANframe(adr=2067478, dat=b"DEADBEEF", tim=1699266191.8744552)
# db.add(tframe)  # добавляем в бд
# db.commit()  # сохраняем изменения

# # print(tom.id)  # можно получить установленный id

# # получение всех объектов
# people = db.query(CANframe).all()
# for p in people:
#     print(f"{p.id}:({p.tim}).{p.adr} ({p.dat})")
