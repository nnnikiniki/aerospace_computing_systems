# Docker на raspberry

Установка docker на raspberry через [apt](https://docs.docker.com/engine/install/raspberry-pi-os/#install-using-the-repository)

На 32-битной версии ОС docker устанавливается по инструкции из ссылки выше, но там не
устанавливается библиотека AstroPy.

На 64-битной версии (Bullseye) докер по инструкции выше не устанавливается. Необходимо
воспользоваться инструкцией аналогичной из
[ссылки](https://blog.alexellis.io/how-to-fix-docker-for-raspbian-buster/). Только надо
заменить ссылки нашу версию ОС:

```bash
wget https://download.docker.com/linux/debian/dists/bullseye/pool/stable/arm64/containerd.io_1.4.10-1_arm64.deb
wget https://download.docker.com/linux/debian/dists/bullseye/pool/stable/arm64/docker-ce-cli_20.10.10~3-0~debian-bullseye_arm64.deb
wget https://download.docker.com/linux/debian/dists/bullseye/pool/stable/arm64/docker-ce_20.10.10~3-0~debian-bullseye_arm64.deb

sudo dpkg -i containerd.io_1.4.10-1_arm64.deb
sudo dpkg -i docker-ce-cli_20.10.10~3-0~debian-bullseye_arm64.deb
sudo dpkg -i docker-ce_20.10.10~3-0~debian-bullseye_arm64.deb

sudo usermod pi -aG docker

```

## Последовательность установки локального кластера kubernetes

kubectl доступен в менеджере пакетов [snap](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-using-other-package-management)
