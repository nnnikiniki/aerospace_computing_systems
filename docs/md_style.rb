# https://github.com/markdownlint/markdownlint/blob/main/example/new_style_example.rb
all
rule 'MD013', :line_length => 88
rule 'MD010', :ignore_code_blocks => true
