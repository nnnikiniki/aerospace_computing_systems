# Приложения

## Raspberry в цикле моделирования

```{.include shift-heading-level-by=2}
raspberry/burn_image_connect_ssh_via_vscode.md
raspberry/physical_can.md
raspberry/raspberry_in_loop.md
```

## Инструкции по работе с кодовой базой

```{.include shift-heading-level-by=2}
repository_mirror.md
guidelines/all_languages.md
guidelines/python.md
guidelines/markdown.md
guidelines/cpp.md
```

<!-- https://github.com/pandoc/lua-filters/tree/master/include-files -->
