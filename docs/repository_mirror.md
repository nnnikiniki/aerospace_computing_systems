# Работа с зеркалированным репозиторием

Для того чтобы коммиты уходили сразу в оба репозитория необходимо изменить файл
.git/config в репозитории. В этом файл должно быть следующее содержимое (обратите
внимание, что используются символы табуляции):

```config
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
[remote "origin"]
	url = git@gitlab.com:Zaynulla/aerospace_computing_systems.git
	url = git@gitflic.ru:zhumaev/acs.git
	fetch = +refs/heads/*:refs/remotes/origin/*
	pushurl = git@gitlab.com:Zaynulla/aerospace_computing_systems.git
	pushurl = git@gitflic.ru:zhumaev/acs.git
[branch "main"]
	remote = origin
	merge = refs/heads/main
[branch "devel"]
	remote = origin
	merge = refs/heads/devel
[pull]
	rebase = true
```
