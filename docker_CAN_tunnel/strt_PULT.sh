#!/bin/bash

CNT=SC_PULT
VXD=vxc1
VCB=vcan1
VCO=vcan0

echo start container $CNT $VXD $VCB $VCO $REG
# $CNT - container name
# $VXD - vxcan in docker
# $VCB - can device in host (can0 or vcan0 etc.) Must be started
# $VCO - can device out host (can0 or vcan0 etc.) Must be started
# $REG - clt program regime
# vxcan1 - this name can in docker

# Если скрипт запускали ранее, то необходимо остановить и удалить соответствующий
# контейнер
# https://stackoverflow.com/a/38225298
docker stop $CNT || true && docker rm $CNT || true

# https://stackoverflow.com/a/46527536
# https://stackoverflow.com/a/34300129
cd ..
#docker run -v "/$(pwd)/capy:/home/capy" -v "/$(pwd)/pult:/home/pult" -v "/$(pwd)/cant:/home/cant" --rm -it -d --name $CNT dpult sh
docker run -v "/$(pwd)/pult:/home/pult" -p 8000:8000 --rm -it -d --name $CNT sc_pult sh
# возврат обратно нужен для возможности простого повторного запуска скрипта
cd -

DPID=$(docker inspect -f '{{ .State.Pid }}' $CNT)
echo ID=$DPID
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
sudo ip link add $VXD type vxcan peer name vxcan1 netns $DPID
echo link OK

sudo ip link set $VXD up
echo "++++++++++++++++++++U " $VXD " OK++++++++++++++++++++++++++++++++"
sudo modprobe can-gw
echo "++++++++++++++++++++++"  $VCB "===" $VXD "++++++++++++++++++++++++++++++++++++++"
sudo cangw -A -s $VCB -d $VXD -e
echo "++++++++++++++++++++++"  $VXD "===" $VCO "++++++++++++++++++++++++++++++++++++++"
sudo cangw -A -s $VXD -d $VCO -e

echo gate ok
# nsenter - name space enter
echo "++++++++++++++++++++++++ up vxcan1 in container ++++++++++++++++++++++++++++++++"
sudo nsenter -t $DPID -n ip link set vxcan1 up
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"


docker exec $CNT bash -c "python /home/pult/app/main.py vxcan1"
##docker exec $CNT bash -c "cd /home"

unset DPID
unset CNT
unset VXD
unset VCB
unset VCO

