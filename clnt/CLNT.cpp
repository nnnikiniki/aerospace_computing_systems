// Client for CAN bus

#include <fstream>
#include <iostream>
#include <unistd.h>

#include <sys/socket.h>
#include <sys/types.h>

#include <netinet/in.h>

#include <string.h>
#include <time.h>

#include <array>
#include <vector>
#include <cstdio>
#include <memory>
#include <stdexcept>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "cans.h"
#include "cant.h"

#define THIS_NODE_BUS_ID 11
#define BROADCAST_NODE_BUS_ID 31

using namespace std;

const std::vector<std::string> StrName
    = { "Repeat",   "Flags",      "ID_CRC8", "Reciever_NET",
        "Reciever", "Sender_NET", "Sender",  "Type" };
const std::vector<uint8_t> StrLen = { 1, 5, 8, 2, 5, 2, 5, 1 };
const std::vector<bool> StrKey = { 0, 0, 0, 1, 1, 0, 0, 0 };

const int FnameLength = 7;//макс длина имени файла в байтах
const int datagramLength=4;//длина датаграммы для передачи файла в байтах

// выполнение команды с возвратом результата в строке
// длина вывода 128 символов

string CMDexec(const char *cmd)
{
    array<char, 128> buffer;
    string result;
    shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe)
        throw runtime_error("popen() failed!");
    while (!feof(pipe.get()))
        {
            if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
                result += buffer.data();
        }
    return result;
}; // string CMDexec(const char* cmd)

// выдается временной маркер (число миллисекунд от события)
uint32_t GETTICK()
{
    struct timespec curtm;
    clock_gettime(CLOCK_REALTIME, &curtm);
    // cout<<"Get Tick!!! "<<" nsec "<<curtm.tv_nsec<<endl;
    return (uint32_t)curtm.tv_nsec;
}; // (*getTick)();

bool SetRecID = false;
uint8_t CurRecID = THIS_NODE_BUS_ID;
uint8_t CurRecNetID = 0;

class VirtualFile{

    public:

    int FileId;
    int FileLength;

    int FrameCount;
    int RestSize;

    char fn[16];
    vector <uint8_t> body;

    VirtualFile(){
        FileLength=-1;
        FileId=-1;
        FrameCount=-1;
        RestSize=-1;
        body.clear();
    }//constructor


    bool isFilled(){
        return (FileLength==body.size());
    }// isFilled

    void CalculateFrameCount(){

        FrameCount=FileLength/datagramLength;
        RestSize=FileLength%datagramLength;

    }//CalculateFrameCount

    void PrintParam(){

        std::cout<<endl<<"File:"<<fn<<endl;
        std::cout<<"Length="<<FileLength<<" Id="<<FileId<<" FrameCount="<<FrameCount<<" RestSize="<<RestSize<<endl;

    }//PrintParam()


    ~VirtualFile(){

    }//destructor



};//class VirtualFile



int main(int argc, char **argv)
{
    bool FINE = false; // признак выхода из цикла

    string CltVer = "version 0.3 acs 2023-05-28";
    const int Nrid = 8;
    string RIDHELP[Nrid + 1];
    RIDHELP[0] = "HELP";
    RIDHELP[1] = "frame monitor";
    RIDHELP[2] = "frame generator";
    RIDHELP[3] = "transmit frame sequence";
    RIDHELP[4] = "transmit file";
    RIDHELP[5] = "transmit command RecieverID code DATA 8 char max";
    RIDHELP[6] = "recieve files and commands";
    RIDHELP[7] = "time syncronization";
    RIDHELP[8] = "PUTTY-CAN";

    const int NcmdH = 250;
    const int NcmdB = 48;
    string CMDHELP[NcmdH + 1];
    for (int i = 0; i < NcmdH + 1; i++)
        CMDHELP[i] = "";
    CMDHELP[48] = "синхронизация";
    CMDHELP[55] = "выполнение системной команды bash на сервере";
    CMDHELP[59] = "прислать файл (имя 7 символов) с ноды №";
    CMDHELP[60] = "команда для режима PUTTY: выполнить файл cmf и вернуть rzf";
    CMDHELP[61] = "команда для режима PUTTY: cmf выполнен rzf отослан";
    CMDHELP[62] = "выполнить командный файл (имя с параметрами 8 символов), "
                  "результат 8 символов вернется в режиме команды 63";
    CMDHELP[63] = "вывести на клиенте содержимое фрейма (8 символов)";
    CMDHELP[64] = "вернуть из файла <имя> 8 байт начиная с заданного с "
                  "возвратом команды 65";
    CMDHELP[65] = "записать 8 байт из фрейма в файл L65";
    CMDHELP[100] = "сделать фотографию";

    bool PUTTYin = false; // признак работы в режиме PUTTY
    bool CMD59 = false;   // признак выдачи команды 59
    bool CMD60 = false;   // признак выдачи команды 60
    bool CMD61 = false;   // признак выдачи команды 61

    // Отправка файла
    char fn[16] = "cmf"; // имя файла

    int FileId = 1; // Id файла 0...255

    int FileSendStep = 0; // число шагов отсылки большого файла (0 - последний шаг)
    int FilePos = 0; // позиция в файле

    vector <VirtualFile> Files;
    Files.clear();




    int nodeID = 11;
    int netID = 0;

    int RxID = 12;
    int RxNetID = 0;

    int SubsNo=0;

    std::string DevName = "can0";

    std::cout << "hello I am client! ";
    std::cout << argc << " : ";
    for (int i = 0; i < argc; i++)
        {
            std::cout << argv[i] << " | ";
        };
    std::cout << endl;

    string aa = " is my FUNCTION";

    int RID = 0;//Режим работы клиента


       if (argc == 1)
        {
            std::cout << "Start without parameters in Help mode!" << endl;
            std::cout << "Parameters (1 max): Config_file" << endl;
            std::cout << CltVer << endl;
            std::cout << "Default Device Name = " << DevName << endl;

            std::cout << endl << " PARAMETERS: " << endl;
            std::cout << RID << " " << RIDHELP[RID] << " " << aa << endl;

            for (int i = 1; i <= Nrid; i++)
                std::cout << i << " - " << RIDHELP[i] << endl;

            std::cout << endl << " COMANDS: " << endl;

            for (int i = NcmdB; i <= NcmdH; i++)
                std::cout << i << " - " << CMDHELP[i] << endl;

            exit(100);
        };//if (argc == 1)

        cans SocCAN;           // объект сокета
        cant FR(StrName, StrLen, StrKey); //объект фрейма

        // разбор параметров

        //string ConfFileName

        std::ifstream in(argv[1]); // окрываем файл для чтения
        if (in.is_open())
        {
            printf("Config Structure Loaded from file! \n");
            in>>DevName;
            in>>RID;
            in>>SocCAN.KeyCount;

            if(SocCAN.KeyCount!=(int)FR.getKeyCount())
                      {
                          std::cout<<"SocCAN.KeyCount:"<<SocCAN.KeyCount<<" != FR.getKeyCount():"<<(int)FR.getKeyCount()<<endl;
                          exit(101);
                      };

            in>>SocCAN.SubScount;

            std::cout<<"Loaded: Dev="<<DevName<<" KeyCount="<<SocCAN.KeyCount<<" SubScount="<<SocCAN.SubScount<<endl;
            std::cout << " RID="<<RID << " " << RIDHELP[RID] << " " << aa << endl;

            vector <int> channel;


            for(int i=0; i<SocCAN.SubScount;i++){
                channel.clear();
                int a;
               for(int k=0; k<2*SocCAN.KeyCount;k++){
                   in>>a;
                   channel.push_back(a);
               };//for k
               std::cout<<i<<" : "<<channel.size()<<endl;
               SocCAN.SubS.push_back(channel);
            };//for i
            std::cout<<" <> "<<SocCAN.SubS.size()<<endl;

            SocCAN.PrintSubS();

        } else {
           std::cout<<"Config File "<<argv[1]<<" NOT FOUND!!"<<endl;
           exit(101);
        };//else if
        in.close(); // закрываем файл

        // разбор параметров


            if (RID == 4)
                { // вводим имя файла. сделано вне конвейера, чтобы не мешать режиму PUTTY

                    printf("TRANSMIT_FILE \n");

                    int YN = 0;
                    while (YN != 1)
                        {

                           // printf("INPUT RecieverID \n");
                           // scanf("%d", &RxID);

                           // printf("INPUT Reciever netID \n");
                           // scanf("%d", &RxNetID);

                           SocCAN.PrintSubS();

                                printf("INPUT Subscribe No \n");
                                scanf("%d", &SubsNo);

                            printf("INPUT File name %d char max and ENTER\n",FnameLength);

                            string str = "";

                            cin.ignore();
                            getline(cin, str);

                            int LS = str.length();

                            std::cout << "input" << str << " (length-1)=" << LS<< endl;

                            for (int k = 0; k < FnameLength; k++)
                                {
                                    if (k < LS)
                                        fn[k] = str[k];
                                    else
                                        fn[k] = 0;
                                };


                            printf("INPUT File Id (1...255) \n");
                            scanf("%d", &FileId);

                            if(FileId==0) FileId=1;
                            if(FileId>255) FileId=255;

                            SocCAN.PrintSub(SubsNo);


                            printf("FileId=%d fn=[%s] Ok (1/0)?\n",FileId, fn);

                            //printf("RxID=%d, RxNetID=%d, FileId=%d fn=[%s] Ok (1/0)?\n",RxID, RxNetID, FileId, fn);

                            scanf("%d", &YN);
                            printf("YN=%d \n", YN);

                        }; // while(YN!='y')

                }; // if RID=4








    SocCAN.start(DevName); // старт сокета

    // ОБЩИЕ ПЕРЕМЕННЫЕ КОНВЕЙЕРА

    int steps = 0;

    int rlen;



    // ГЛАВНЫЙ КОНВЕЙЕР++++++++++++++++++++++++++++++++++++++++++++++++++
    while (!FINE)
        {

            switch (RID)
                { // По номеру режима

                case 1:
                    {
                        // cout<<"Monitor"<< endl;
                        //*

                        int REZ = 0;
                        REZ = SocCAN.recvFrame(&FR, 1, 100);

                        if (REZ < 0)
                            {
                                std::cout << "recv ERROR!!!" << endl;
                                exit(999);
                            };

                        if (REZ > 0)
                            {
                                std::cout << "After receive " << endl;
                                FR.PrintFrame();

                            }; //

                        std::cout << "stp=" << steps << " rlen=" << REZ << " I ";
                        //*/
                        // Здесь монитор только принятых кадров
                    }
                    break;
                //===========================2222222222222222222222222==================
                case 2:
                    {
                        //*
                        printf(" cant FR start!!!\n");
                        cant FR(StrName, StrLen, StrKey);

                        printf(" FR.dlc=%d\n", FR.dlc);

                        FR.setAttrByName("Type", 0);
                        FR.setAttrByName("Sender", nodeID);
                        FR.setAttrByName("Sender_NET", 0);
                        FR.setAttrByName("Reciever", RxID);
                        FR.setAttrByName("Reciever_NET", 0);
                        FR.setAttrByName("ID_CRC8", 0);
                        FR.setAttrByName("Flags", 0);
                        FR.setAttrByName("Repeat", 0);

                        FR.codeId();

                        std::cout << "Adr: " << endl;

                        bitset<32> x(FR.getAdr());
                        bitset<32> y(FR.getAdr());
                        bitset<32> b(CAN_EFF_FLAG);

                        std::cout << " CAN_EFF_FLAG = " << b << '\n';
                        std::cout << "bit CAN    ID = " << x << '\n';
                        std::cout << "bit CAN EFFID = " << y << '\n';

                        FR.decodeId();

                        bitset<32> z(FR.getAdr());
                        bitset<32> z1(FR.frm.can_id);

                        std::cout << "bit CAN   .ID = " << z << '\n';
                        std::cout << "bit CANfrm.ID = " << z1 << '\n';

                        int f = FR.getAttrByName("Flags");
                        std::cout << "Flags=" << f << endl;

                        printf("id=%d \n", FR.getAdr());

                        for (int k = 0; k < 8; k++)
                            FR.frm.data[k] = 0;
                        FR.frm.data[0] = steps;

                        FR.PrintFrame();

                        int bytes_send = 0;

                        bytes_send = SocCAN.sendFrame(FR);
                        std::cout << "bytes_send =" << bytes_send << endl;

                        sleep(5);
                        //*/
                        std::cout << "SEND TO SERVER OK!!! \n";
                    }
                    break;

                //===========================3333333333333333333333333==================

                case 3:
                    {
                        printf("TRANSMIT_FRAME \n");

                        /*
                        bus_adress snd;
                        snd.netID = 0;
                        snd.nodeID = 3;

                        bus_adress rcv;
                        rcv.netID = 0;
                        rcv.nodeID = 11;

                        RxFRAME.type = 0;
                        RxFRAME.sender = snd;
                        RxFRAME.receiver = rcv;
                        RxFRAME.source = 0;
                        RxFRAME.flags = 0;
                        RxFRAME.isRepeat = 0;
                        RxFRAME.ID_CRC8 = 1;
                        for (int k = 1; k < 9; k++)
                                RxFRAME.data[k] = 0;
                        RxFRAME.data[0] = steps;
                        RxFRAME.data[1] = 11;

                        PrintFrames(&RxFRAME);
                        printf("TRANSMIT_FRAME: socket=%d socanUB=%d \n",
                        socket, 1); transmit_frame(&RxFRAME, 1, 100);
                        // user_bus_tx_thread();//это перенесено в конец
                        printf("TRANSMIT_FRAME OK \n");
                        //*/
                        sleep(5);
                    }
                    break;
                //===========================4444444444444444444444444==================
                case 4:
                    {
                        // 06.10.2022 ВНИМАНИЕ! Пересылается только файл длиной
                        // не более PartL!!!

                        // RxID, RxNetID, fn

                        //*
                        printf("TRANSMIT_FILE: name=%s Id=%d \n", fn, FileId);

                        size_t FileLength;
                        FILE *cur_file;

                        if ((cur_file = fopen(fn, "r")) == NULL)
                            {
                                printf("open TRANSMITED file RID=4 ERROR!!!\n");
                            }
                        else
                            {
                                int bytes_send;
                                printf("open Ok ");
                                fseek(cur_file, 0, SEEK_END);
                                FileLength = ftell(cur_file);
                                fseek(cur_file, 0, SEEK_SET);


                                std::cout << " FileLength " << FileLength << endl;

                                int FrameCount=(FileLength)/datagramLength;
                                int FinalDatagramLen=(FileLength)%datagramLength;

                                printf("FrameCount=%d FinalDatagramLen=%d\n", FrameCount, FinalDatagramLen);

                                printf("Receiver NetID=%d NodeID=%d\n", RxNetID, RxID);

                                FilePos = 0;
                                //заготовка адреса

                                printf(" cant FR start!!!\n");
                                //cant FR(StrName, StrLen, StrKey);

                                printf(" FR.dlc=%d\n", FR.dlc);

                                FR.setAttrByName("Type", 1);
                                //FR.setAttrByName("Sender", nodeID);
                                //FR.setAttrByName("Sender_NET", 0);
                                //FR.setAttrByName("Reciever", RxID);
                                //FR.setAttrByName("Reciever_NET", 0);

                                FR.setAttrByName("Flags", 0);
                                FR.setAttrByName("Repeat", 0);

                                FR.codeId();

                                //собственно передача файлов
                                // заголовок
                                   FR.setAttrByName("ID_CRC8", 0);
                                   FR.frm.data[0]=FileId;
                                   for(int i=0;i<FnameLength;i++)
                                                FR.frm.data[i+1]=fn[i];
                                   FR.codeId();
                                   printf("Head \n");
                                   //FR.PrintFrame();
                                   //bytes_send = SocCAN.sendFrame(FR);
                                   bytes_send = SocCAN.sendFrameSub(FR,SubsNo,true);
                                   std::cout << "bytes_send =" << bytes_send << endl;
                                // длина
                                   FR.setAttrByName("ID_CRC8", FileId);

                                   FR.inputInt32(0,0);//segment No
                                   FR.inputInt32(FileLength,datagramLength);//data


                                   FR.codeId();
                                   printf("Length \n");
                                   //FR.PrintFrame();
                                   //bytes_send = SocCAN.sendFrame(FR);
                                   bytes_send = SocCAN.sendFrameSub(FR,SubsNo,true);
                                   std::cout << "bytes_send =" << bytes_send << endl;

                                // тело
                                uint8_t buf[datagramLength];
                                FilePos=0;

                                printf("Body \n");
                                for(int pos=0; pos<FrameCount;pos++)
                                {

                                   FR.inputInt32(pos+1,0);
                                   fread(buf, sizeof(uint8_t), datagramLength, cur_file);
                                   for(int i=0;i<datagramLength;i++)
                                       FR.frm.data[i+datagramLength]=buf[i];

                                   //FR.PrintFrame();
                                   //bytes_send = SocCAN.sendFrame(FR);
                                   bytes_send = SocCAN.sendFrameSub(FR,SubsNo,true);
                                   std::cout << "bytes_send =" << bytes_send << endl;
                                }
                                //заключительный кадр
                                printf("Tail \n");
                                if(FinalDatagramLen>0)
                                {
                                   for(int i=0;i<datagramLength;i++)
                                       FR.frm.data[i+4]=0;
                                   FR.inputInt32(FrameCount+1,0);
                                   fread(buf, sizeof(uint8_t), FinalDatagramLen, cur_file);
                                   for(int i=0;i<FinalDatagramLen;i++)
                                       FR.frm.data[i+4]=buf[i];

                                   //FR.PrintFrame();
                                   //bytes_send = SocCAN.sendFrame(FR);
                                   bytes_send = SocCAN.sendFrameSub(FR,SubsNo,true);
                                   std::cout << "bytes_send =" << bytes_send << endl;
                                }

                                printf(" cant FR finish!!!\n");

                                fclose(cur_file);
                            }; // else if ((cur_file


                        if (FileSendStep == 0)
                            {
                                // printf("TRANSMIT_FILE OK \n");
                                RID = 100;

                                if (PUTTYin)
                                    {
                                        RID = 8;
                                        CMD60 = true;
                                    };

                                if (CMD61)
                                    {
                                        RID = 8;
                                        SetRecID = true;
                                        // printf("SET in RID=4 with CMD61=true
                                        //                          RID = 8 ");
                                    }; // if(CMD61)

                                if (CMD59)
                                    {
                                        RID = 6;
                                        CMD59 = false;
                                        // printf("SET in RID=4 with
                                        // CMD59=true RID = 6 ");

                                    }; // if(CMD59)

                            }; // if(FileSendStep==0)

                        //*/
                        // sleep(3);
                    }
                    break;
                //===============================55555555555555555555===================
                case 5:
                    {
                        printf("TRANSMIT_COMMAND \n");

                        //*

                        char dat[9];
                        int RxID = 11;
                        int RxNetID = 0;
                        int CmCd;
                        int YN = 0;



                        while (YN != 1)
                            {


                                for (int k = 0; k < 9; k++)
                                    dat[k] = 0;

                                SocCAN.PrintSubS();

                                printf("INPUT Subscribe No \n");
                                scanf("%d", &SubsNo);

                                //printf("INPUT Reciever nodeID \n");
                                //scanf("%d", &RxID);
                                //printf("INPUT Reciever netID \n");
                                //scanf("%d", &RxNetID);
                                printf("INPUT commandcode \n");
                                scanf("%d", &CmCd);

                                if (NcmdB <= CmCd <= NcmdH)
                                    std::cout << CMDHELP[CmCd] << endl;

                                switch (CmCd)
                                    { // По номеру режима

                                    case 64:
                                        {
                                            /*
                                            for (int k = 0; k < 8; k++)
                                            {
                                                    dat[k] = 0;

                                            }; // for k

                                            printf("TRANSMIT 8 bytes from file\n");
                                            printf("INPUT File NAME %d char max and ENTER\n",FnameLength);
                                            string str= "";

                                            cin.ignore();
                                            getline(cin, str);

                                            int LS = str.length();
                                            //(str.length()<=8)?str.length():8;
                                            cout << "LS= " << LS << endl;

                                            for (int k = 0; k < FnameLength; k++)
                                            {
                                                    if (k < LS)
                                                    {
                                                            dat[k] = str[k];
                                                    }
                                                    else
                                                    {
                                                            dat[k] = 0;

                                                    }; // if
                                            };	   // for k

                                            printf("INPUT Start adress\n");
                                            int StAdr; scanf("%d",&StAdr);

                                            //if (StAdr > 120) StAdr = 120; dat[4] = StAdr;

                                            printf("RxNetID=%d, RxNodeID=%d,CmCd=%d, dat=[%c%c%c%c] adr=[%d] Ok(1/0)?\n", RxNetID, RxID, CmCd,dat[0], dat[1], dat[2], dat[3],dat[4]);
                                            //*/
                                        }
                                        break; // CmCd==64

                                    default:
                                        {

                                            printf("INPUT DATA 8 char max and ENTER\n");

                                            string str = "";

                                            cin.ignore();
                                            getline(cin, str);

                                            // cout<<"Input "<<str<<endl;
                                            int LS
                                                = str.length(); //(str.length()<=8)?str.length():8;
                                            // cout<<"LS= "<<LS<<endl;

                                            for (int k = 0; k < 8; k++)
                                                {
                                                    if (k < LS)
                                                        dat[k] = str[k];
                                                    else
                                                        dat[k] = 0;

                                                }; // for k

                                            //SocCAN.PrintSubS();
                                            SocCAN.PrintSub(SubsNo);

                                            printf("SubsNo=%d CmCd=%d, dat=[%s] Ok(1/0)?\n",SubsNo, CmCd, dat);
                                        }
                                        break; // default CmCd

                                    }; // case CmCd

                                scanf("%d", &YN);
                                printf("YN=%d \n", YN);
                            }; // while

                        //cant FR(StrName, StrLen, StrKey);

                        FR.setAttrByName("Type", 0); // command
                        //FR.setAttrByName("Sender", nodeID);
                        //FR.setAttrByName("Sender_NET", 0);
                        //FR.setAttrByName("Reciever", RxID);
                        //FR.setAttrByName("Reciever_NET", RxNetID);
                        FR.setAttrByName("ID_CRC8", CmCd); // comand code
                        FR.setAttrByName("Flags", 0);
                        FR.setAttrByName("Repeat", 0);

                        FR.codeId();


                        for (int k = 0; k < 8; k++)
                                     FR.frm.data[k] = dat[k];

                        //FR.PrintFrame();

                        int bytes_send = 0;

                        //bytes_send = SocCAN.sendFrame(FR);
                        bytes_send = SocCAN.sendFrameSub(FR,SubsNo,1);

                        if (bytes_send < 0)
                            {
                                std::cout << "ERROR CANSEND SERVER!!! " << endl;
                                sleep(1);
                            };

                        std::cout << "bytes_send =" << bytes_send << endl;

                        printf("TRANSMIT_FRAME OK \n");
                        //*/
                        // sleep(5);
                    }
                    break; // case 5

                //===============================6666666666666666666====================
                case 6:
                    {
                        //*
                        printf("WAIT FOR RECIEVE_FILE_COMMAND: \n", socket, 1);

                        int REZ = 0;
                        REZ = SocCAN.recvFrame(&FR, 1, 100);

                        REZ = SocCAN.recvFrameSub(&FR, REZ, 100);

                        if (REZ >= 0)
                            {
                                std::cout << "After receive " << "stp=" << steps << " rlen=" << REZ << " I " << endl;
                                FR.PrintFrame();

                                if (FR.getAttrByName("Type") == 0)
                                    { //----------------COMMAND!

                                        int CmCode
                                            = (int)FR.getAttrByName("ID_CRC8");

                                        int snd_netID
                                            = FR.getAttrByName("Sender_NET");
                                        int snd_nodeID
                                            = FR.getAttrByName("Sender");

                                        bool CmdPASSED = ((CmCode >= 50)
                                                          && (CmCode < 100));
                                        /*
                                            if(CmdPASSED) { // квитанция о получении

                                            TxF= bff;

                                            cout<< "KVITOK 01!!! "; //<<endl;

                                           TxF.receiver.nodeID= bff.sender.nodeID;
                                           TxF.receiver.netID = bff.sender.netID;

                                           TxF.type = 0; TxF.ID_CRC8 = 47;

                                            for(int j = 0; j < 8; j++)
                                                               TxF.data[j] = 0;

                                           TxF.data[0]= CmCode;
                                           TxF.data[1] = 82; //'R'
                                           PrintFrames(&TxF);
                                           //
                                           cout<<" Kvitok 01 OK!!! "<<endl;
                                           transmit_frame(&TxF,1, 100); // пересылка по шине CAN
                                           };//  if(CmdPASSED)
                                        //*/

                                        if (CmdPASSED || (CmCode == 47))
                                            { // обработка команды
                                                std::cout<< "RECEIVED COMMAND CODE="<< CmCode << endl;

                                                switch (CmCode)
                                                    {

                                                    case 47:
                                                        {

                                                            string cmd47 = "";

                                                            for (int i = 0; i < 8; i++)
                                                                {
                                                                    cmd47+= FR.frm.data[i];
                                                                }; // for

                                                            std::cout << "COMMAND 47: Frame from nodeID="<< (int)snd_nodeID<< " netID="<< (int)snd_netID<< " DATA="<< cmd47<< endl;

                                                            sleep(1);
                                                        }
                                                        break; // cmd 47

                                                    case 59:
                                                        {
                                                            //*
                                                            for (int i = 0; i < FnameLength; i++) fn[i] =FR.frm.data[i];

                                                            std::cout << "COMMAND 59: Send file name=" << fn << endl;

                                                            // printf("cmf file name=%s\n",fn);

                                                            size_t FileLength;
                                                            FILE *cur_file;
                                                            if ((cur_file = fopen(fn, "r")) == NULL)
                                                            {
                                                                    printf("cmd 59 CMF file open ERROR\n");
                                                            }
                                                            else
                                                            {
                                                                    printf("open Ok ");
                                                                    fseek(cur_file, 0, SEEK_END);
                                                                    FileLength = ftell(cur_file);
                                                                    fseek(cur_file, 0, SEEK_SET);
                                                                    std::cout << " FileLength "<< FileLength <<endl;
                                                                    if (FileLength == 0)
                                                                        fwrite("ERROR", sizeof("ERROR"), 1, cur_file);
                                                                    fclose(cur_file);
                                                            }; // if

                                                            //RxNetID=FR.getAttrByName("Sender_NET");
                                                            //RxID=FR.getAttrByName("Sender");

                                                            SubsNo=REZ;



                                                            CMD59 = true;

                                                            RID = 4;
                                                            // printf("Next RID=4\n");
                                                            //*/
                                                        }
                                                        break; // cmd 59

                                                    case 60:
                                                        {
                                                            /*
                                                            cout << "COMMAND 60: EXEC CMF!!!" <<endl;

                                                            system("rm rzf");
                                                            system("cd .");
                                                            cout << " rm rzf "<< endl;

                                                            fn[0] = 'c';
                                                            fn[1] = 'm';
                                                            fn[2] = 'f';
                                                            fn[3] = 0;

                                                            printf("cmf file name=%s\n", fn);

                                                            size_t FileLength;
                                                            FILE *cur_file;
                                                            if ((cur_file =fopen(fn, "r")) ==NULL)
                                                            {
                                                                    printf("cmd 60 CMF file open ERROR\n");
                                                            }
                                                            else
                                                            {
                                                                    printf("open Ok ");
                                                                    fseek(cur_file,0, SEEK_END);
                                                                    FileLength = ftell(cur_file);
                                                                    fseek(cur_file, 0, SEEK_SET);
                                                                    cout<< " FileLength "<< FileLength <<endl;
                                                                    fclose(cur_file);
                                                            }; // if

                                                            system("chmod 777 cmf"); cout << "chmod " << endl;

                                                            system("./cmf>>rzf");
                                                            cout << " cmf>>rzf" << endl;
                                                            system("cd .");
                                                            cout << " refresh "<< endl;

                                                            cout << " FILE WAS EXECUTED! " <<endl;

                                                            // возврат результата
                                                            cout <<"RETURN RZF!!!" <<endl;

                                                            if ((cur_file =fopen("rzf", "r"))== NULL)
                                                            {
                                                                    printf("RZF file open ERROR\n");
                                                            }
                                                            else
                                                            {
                                                                    printf("rzf open Ok ");
                                                                    fseek(cur_file,0, SEEK_END);
                                                                    FileLength= ftell(cur_file);
                                                                    fseek(cur_file,0, SEEK_SET);
                                                                    cout<< "rzf FileLength" << FileLength <<endl;
                                                                    if(FileLength == 0)
                                                                            fwrite("ERROR",sizeof("ERROR"), 1,cur_file);
                                                                    fclose(cur_file);
                                                            }; // else if

                                                            rcv.netID =snd.netID;
                                                            rcv.nodeID =snd.nodeID; CMD61 =true;

                                                            fn[0] = 'r';
                                                            fn[1] = 'z';
                                                            fn[2] = 'f';
                                                            fn[3] = 0;

                                                            printf("rez file name=%s\n", fn);

                                                            RID = 4;
                                                            // printf("Next RID=4\n");
                                                            //*/
                                                        }
                                                        break; // cmd 60

                                                    case 61:
                                                        {
                                                            /*
                                                            cout << "COMMAND 61
                                                            show rzf file!!!"
                                                            << endl;

                                                            string x;
                                                            ifstream inFile;

                                                            inFile.open("rzf");
                                                            if (!inFile)
                                                            {
                                                                cout <<"Unable to open rzf file";
                                                                // exit(1);
                                                                // terminate with error
                                                            }
                                                            else
                                                            {

                                                                    while(getline(inFile,x))
                                                                    {
                                                                        cout<< x << endl;
                                                                    }

                                                                    inFile.close();

                                                                    system("mvrzf rzo"); //
                                                                    system("rm rzf");
                                                                    system("cd ."); cout << endl<<endl<<" rename rzf after show rzf" << endl<<endl;
                                                            };

                                                            if (!PUTTYin)
                                                            {
                                                                    PUTTYin =true;
                                                                    RID = 8;
                                                            };
                                                            */
                                                        }
                                                        break; // cmd 61

                                                    case 62:
                                                        {

                                                            string cmd62= "./";

                                                            for (int i = 0;i < 8; i++)
                                                                {
                                                                    cmd62+= FR.frm.data[i];
                                                                }; // for

                                                            std::cout<< "COMMAND 62: EXEC file="<< cmd62.c_str()<< endl;
                                                            string rezult;
                                                            rezult = CMDexec(cmd62.c_str());
                                                            int LR= rezult.length()- 1;
                                                            std::cout << "rezultat="<< rezult<< "Length="<< LR << endl;

                                                            cant FRet(StrName,StrLen, StrKey);

                                                            FRet.setAttrByName("Type",0); // command
                                                            //FRet.setAttrByName("Sender",nodeID);
                                                            //FRet.setAttrByName("Sender_NET",0);
                                                            //FRet.setAttrByName("Reciever",snd_nodeID);
                                                            //FRet.setAttrByName("Reciever_NET",snd_netID);
                                                            FRet.setAttrByName("ID_CRC8",63); // comand code
                                                            FRet.setAttrByName("Flags", 0);
                                                            FRet.setAttrByName("Repeat", 0);

                                                            FRet.codeId();

                                                            for (int k = 0;k < 8; k++)
                                                                {

                                                                    if (k < LR)
                                                                        FRet.frm.data[k]= rezult[k];
                                                                    else
                                                                        FRet.frm.data[k]= 0;
                                                                    // cout<<"data:
                                                                    // "<<RxFRAME.data[k]<<endl;
                                                                };



                                                            //FRet.PrintFrame();

                                                            int bytes_send = 0;

                                                            //bytes_send= SocCAN.sendFrame(FRet);
                                                            bytes_send= SocCAN.sendFrameSub(FRet,REZ,true);

                                                            if (bytes_send < 0)
                                                                {
                                                                    std::cout
                                                                        << "ERROR CANSEND SERVER!!! "
                                                                        << endl;
                                                                    sleep(1);
                                                                };

                                                            std::cout << "bytes_send ="<< bytes_send<< endl;

                                                            printf("TRANSMIT_FRAME OK \n");

                                                            // sleep(5);
                                                        }
                                                        break; // cmd 62

                                                    case 63:
                                                        {
                                                            //*
                                                            string cmd63 = "";

                                                            for (int i = 0;i < 8; i++)
                                                                {
                                                                    cmd63+= FR.frm.data[i];
                                                                }; // for

                                                            std::cout<< "COMMAND 63: Frame from nodeID="
                                                                << (int)FR.getAttrByName("Sender")
                                                                << " netID="<< (int)FR.getAttrByName("Sender_NET")
                                                                << " DATA="<< cmd63<< endl;

                                                            sleep(1);
                                                            //*/
                                                        }
                                                        break; // cmd 63

                                                    case 64:
                                                        {
                                                            /*
                                                            char ddt[8];
                                                            fn[0] = 0;
                                                            fn[1] = 0;
                                                            fn[2] = 0;
                                                            fn[3] = 0;

                                                            for (int i = 0; i <4; i++)
                                                                 if(bff.data[i] == 32)
                                                                       i =4;
                                                                 else
                                                                       fn[i] = bff.data[i];

                                                            int StAdr = bff.data[4];

                                                            cout << "COMMAND 64: Send 8 bytes from file name=" <<fn << " Starting "<< StAdr << endl;

                                                            printf("cmf file name=%s\n", fn);

                                                            size_t FileLength;
                                                            FILE *cur_file;
                                                            if ((cur_file =fopen(fn, "r")) ==NULL)
                                                            {
                                                                printf("cmd 64 CMF file openERROR\n");
                                                            }
                                                            else
                                                            {
                                                                printf("openOk ");
                                                                fseek(cur_file,0, SEEK_END);
                                                                FileLength= ftell(cur_file);
                                                                fseek(cur_file,0, SEEK_SET);
                                                                cout<< " FileLength "<< FileLength <<endl;
                                                                if(FileLength == 0)
                                                                            printf("ERROR\n");

                                                                if (StAdr >(FileLength - 1))
                                                                            StAdr= 0;

                                                                fseek(cur_file,StAdr, SEEK_SET);

                                                                fread(ddt,8, 1, cur_file);

                                                                    for (int k= 0; k < 8; k++)
                                                                            printf("%d", ddt[k]);
                                                                    printf("\n");

                                                                    fclose(cur_file);
                                                            }; //else  if

                                                            bus_adress snd;
                                                            snd.netID =UBCFG.thisNetID;
                                                            snd.nodeID =UBCFG.thisNodeID;
                                                            bus_adress rcv;
                                                            rcv.netID =bff.sender.netID;
                                                            rcv.nodeID =bff.sender.nodeID;

                                                            RxFRAME.type = 0;// command frame
                                                            RxFRAME.sender =snd;
                                                            RxFRAME.receiver =rcv;
                                                            RxFRAME.source= 0;
                                                            RxFRAME.flags= 0;

                                                            RxFRAME.isRepeat =0;
                                                            RxFRAME.ID_CRC8= 65; // Command CODE

                                                            for (int k =0; k < 8; k++)
                                                            {
                                                                    RxFRAME.data[k]= ddt[k];
                                                            }; // for

                                                            PrintFrames(&RxFRAME);

                                                            //
                                                            printf("TRANSMIT_CMD_63\n");
                                                            transmit_frame(&RxFRAME,1, 100);

                                                            // printf("NextRID=4\n");
                                                            //*/
                                                        }
                                                        break; // cmd 64

                                                    case 65:
                                                        {

                                                            string cmd65 = "";

                                                            for (int i = 0;i < 8; i++)
                                                                {
                                                                    cmd65+= FR.frm.data[i];
                                                                }; // for

                                                            std::cout<< "COMMAND 65: Frame from nodeID="<< (int)snd_nodeID<< " netID="<< (int)snd_netID<< " DATA="<< cmd65<< endl;
                                                            std::ofstream out("L65",std::ios::app);
                                                            if (out.is_open())
                                                                {
                                                                    for (int i= 0;i < 8;i++)
                                                                        {
                                                                            out << FR.frm.data[i];
                                                                        }; // for
                                                                }
                                                            out.close();

                                                            // sleep(1);
                                                        }
                                                        break; // cmd 65

                                                    default:
                                                        {
                                                            std::cout << "UNKNOW COMMAND CODE="<< CmCode<< endl;
                                                        }
                                                        break; // default :

                                                    }; // CASE

                                            }; //  if(CmdPASSED)
                                               /*
                                                   if(CmdPASSED)        { // квитанция о получении

                                                    for(int j = 0; j < 8; j++)
                                                                       TxF.data[j]= 0;

                                                  TxF.data[0]= CmCode;
                                                  TxF.data[1] = 82;//'R'
                                                  TxF.data[2] = 79;//'O'
                                                  cout << " Kvitok 02 OK!!!";//<<endl;
                                                  PrintFrames(&TxF);

                                                  transmit_frame(&TxF,1, 100); // пересылка по шине CAN
                                                                               };
                                                  //  if(CmdPASSED)
                                               */
                                    }
                                else
                                    { //----------------FILE!
                                        //*
                                            std::cout << "REC FILE! files count="<<Files.size() <<endl;
                                            //FR.PrintFrame();
                                            int Attribute = (int)FR.getAttrByName("ID_CRC8");

                                            if(Attribute==0){

                                                std::cout<<"New Head of file !"<<endl;
                                                VirtualFile VF;

                                                VF.FileId=FR.frm.data[0];
                                                for(int i=0;i<FnameLength;i++)
                                                           VF.fn[i]=FR.frm.data[i+1];
                                                VF.PrintParam();

                                                Files.push_back(VF);

                                            }//if Attribute

                                            for(int ff=0;ff<Files.size();ff++){

                                                  if(Files[ff].FileId==FR.getAttrByName("ID_CRC8"))
                                                       {
                                                            if(FR.outputInt32(0)==0){
                                                                 Files[ff].FileLength=FR.outputInt32(4);
                                                                 Files[ff].CalculateFrameCount();
                                                                 Files[ff].PrintParam();

                                                            } else {

                                                             uint32_t FrameNo=FR.outputInt32(0);

                                                             if(FrameNo<=Files[ff].FrameCount){
                                                                for(int k=0;k<datagramLength;k++)
                                                                             Files[ff].body.push_back(FR.frm.data[k+4]);
                                                             } else {

                                                                for(int k=0;k<Files[ff].RestSize;k++)
                                                                             Files[ff].body.push_back(FR.frm.data[k+4]);

                                                             };//else if(FrameNo<=Files[ff].FrameCount)

                                                             std::cout<<"ff="<<ff<<" FilLen="<<Files[ff].FileLength<<" bodySize="<<Files[ff].body.size()<<endl;

                                                             if(Files[ff].isFilled()){

                                                                //for(int j=0;j< Files[ff].body.size();j++){
                                                                //      cout<<  (char)Files[ff].body[j];
                                                                //}

                                                                //cout<<endl;

                                                                 FILE *cur_file;

                                                                if ((cur_file = fopen(Files[ff].fn, "w")) == NULL)
                                                                {
                                                                      printf("open rceived file ERROR!!!\n");
                                                                } else {

                                                                       printf("open Ok ");
                                                                       for(int j=0;j< Files[ff].body.size();j++){
                                                                             fwrite(&Files[ff].body[j],sizeof(uint8_t),1,cur_file);
                                                                        };
                                                                        fclose(cur_file);
                                                                };

                                                                Files.erase(Files.begin()+ff);
                                                                std::cout<<"!!! VirtFile N0="<<ff<<" erased! Files.size="<<Files.size()<<endl;

                                                             }// if is filled


                                                            };// else if(FR.outputInt32(0)==0)

                                                       }
                                                       //else {
                                                        //cout<<"UNKNOWN FILE ID!!!"<<endl;
                                                        //exit(111);
                                                       //}
                                            }//for

                                                // exit(100);

                                                printf("RECIEVE_FILE OK \n");
                                          //*/
                                    }; // if type

                            }; // if REZ>0
                               //*/
                               //  sleep(1);
                    }
                    break; // case 6:

                //====================================777777777777777777================
                case 7:
                    {
                        /*
                        printf("SEND UNIX TIME SYNCRONIZATION FRAME (COM. CODE
                        0x30 or 48\n");

                        int RxID = 11;
                        int RxNetID = 0;

                        printf("INPUT Reciever nodeID \n");
                        scanf("%d", &RxID);
                        printf("INPUT Reciever netID \n");
                        scanf("%d", &RxNetID);

                        // printf("INPUT unix time code\n");

                        // string str="";
                        const int muster = 1643022840;
                        int UTcd = 1643022840;

                        printf("INPUT UNIX Time Code= \n");
                        scanf("%d", &UTcd);
                        if (UTcd == 0)
                        {
                                cout << "UTdc=0 -> Muster must be used! ";
                                UTcd = muster;
                        };

                        // cin.ignore();
                        // getline(cin,UTcd);

                        cout << "input RxID=" << RxID << " RxNetID=" << RxNetID
                        << " Time Code=" << UTcd << endl;

                        //*

                        bus_adress snd;
                        snd.netID = 0;
                        snd.nodeID = 11; // все равно какой адрес - эту цифру
                        задает сервер!

                        bus_adress rcv;
                        rcv.netID = RxNetID;
                        rcv.nodeID = RxID;

                        RxFRAME.type = 0; // command
                        RxFRAME.sender = snd;
                        RxFRAME.receiver = rcv;
                        RxFRAME.source = 0;
                        RxFRAME.flags = 0;
                        RxFRAME.isRepeat = 0;
                        RxFRAME.ID_CRC8 = 0x30;
                        for (int k = 1; k < 9; k++)
                                RxFRAME.data[k] = 0;

                        struct timespec curtm;

                        clock_gettime(CLOCK_REALTIME, &curtm);
                        cout << "current time "
                                 << "sec " << curtm.tv_sec << " nsec " <<
                        curtm.tv_nsec << endl;

                        union
                        {
                                uint32_t val;
                                uint8_t bytes[4];
                        } b_sec, b_nsec;

                        b_sec.val = curtm.tv_sec;
                        b_nsec.val = curtm.tv_nsec;

                        b_sec.val = UTcd;
                        b_nsec.val = 0;

                        RxFRAME.data[0] = b_sec.bytes[0];
                        RxFRAME.data[1] = b_sec.bytes[1];
                        RxFRAME.data[2] = b_sec.bytes[2];
                        RxFRAME.data[3] = b_sec.bytes[3];

                        RxFRAME.data[4] = b_nsec.bytes[0];
                        RxFRAME.data[5] = b_nsec.bytes[1];
                        RxFRAME.data[6] = b_nsec.bytes[2];
                        RxFRAME.data[7] = b_nsec.bytes[3];

                        PrintFrames(&RxFRAME);
                        printf("TRANSMIT_FRAME: socket=%d socanUB=%d \n",
                        socket, 1);

                        transmit_frame(&RxFRAME, 1, 100);
                        // user_bus_tx_thread();//это перенесено в конец
                        // printf("TRANSMIT_FRAME OK \n");
                        //*/
                        // exit(402);
                        // sleep(10);//ОДИН РАЗ в 10 сек
                        FINE = true;
                    }
                    break;

                //=======================================88888888888888888==============
                case 8:
                    {
                        /*
                        printf("PUTTY-CAN \n");
                        // bus_adress rcv;

                        if (!SetRecID)
                        {
                                printf("INPUT Reciever nodeID>");
                                scanf("%d", &CurRecID);
                                cin.ignore();
                                printf("INPUT Reciever netID>");
                                scanf("%d", &CurRecNetID);
                                cin.ignore();

                                printf("RecieverID=%d NetId=%d\n",CurRecID,CurRecNetID);

                                SetRecID = true;
                                PUTTYin = true;
                        };
                        //*/

                        /*
                        if ((!CMD60) && (!CMD61))
                        {
                                string str = "";
                                printf("INPUT command to Reciever nodeID=%d netID=%d and ENTER\n", CurRecID, CurRecNetID);
                                getline(cin, str);
                                cout << "Your input is :[" << str << "]" <<endl;

                                system("chmod 777 cmf");
                                system("rm cmf");
                                system("cd .");

                                ofstream myfile;
                                myfile.open("cmf");
                                myfile << str;
                                myfile.close();

                                system("cd .");

                                RxID= CurRecNetID;
                                RxNetID= CurRecID;

                                fn[0] = 'c';
                                fn[1] = 'm';
                                fn[2] = 'f';
                                fn[3] = 0;

                                printf("FileName=%s\n", fn);

                                PUTTYin = true;
                                RID = 4;

                        }; // if(!CMD60)

                        if (CMD60)
                        {

                                bus_adress snd;
                                snd.netID = UBCFG.thisNetID;
                                snd.nodeID = UBCFG.thisNodeID;

                                rcv.netID = CurRecNetID;
                                rcv.nodeID = CurRecID;

                                RxFRAME.type = 0; // command frame
                                RxFRAME.sender = snd;
                                RxFRAME.receiver = rcv;
                                RxFRAME.source = 0;
                                RxFRAME.flags = 0;
                                RxFRAME.isRepeat = 0;
                                RxFRAME.ID_CRC8 = 60; // Command CODE
                                for (int k = 0; k < 8; k++)
                                        RxFRAME.data[k] = 0;

                                PrintFrames(&RxFRAME);

                                printf("TRANSMIT_CMD_60\n");
                                transmit_frame(&RxFRAME, 1, 100);

                                CMD60 = false;

                                // user_bus_tx_thread();//это перенесено в конец

                                //printf("PUTTY-CAN finish!\n");

                                //if (PUTTYin)
                                //{
                                //	PUTTYin = false;
                                //	RID = 6;
                                //};



                        }; // ifif(CMD60)

                        if (CMD61)
                        {

                                printf("In RID=8 CMD61\n");

                                CMD61 = false;

                                system("mv cmf cmo"); // system("rm cmf");
                                system("cd .");
                                cout << " rename rzf after send rzf" << endl;

                                printf("TRANSMIT_FILE cmd OK \n");

                                bus_adress snd;
                                snd.netID = UBCFG.thisNetID;
                                snd.nodeID = UBCFG.thisNodeID;

                                RxFRAME.type = 0; // command
                                RxFRAME.sender = snd;
                                RxFRAME.receiver = rcv;
                                RxFRAME.source = 0;
                                RxFRAME.flags = 0;
                                RxFRAME.isRepeat = 0;
                                RxFRAME.ID_CRC8 = 61; // команда возврата результата выполнения

                                for (int k = 0; k < 8; k++)
                                        RxFRAME.data[k] = 0;

                                PrintFrames(&RxFRAME);

                                printf("TRANSMIT_CMD_61\n");
                                transmit_frame(&RxFRAME, 1, 100);

                                RID = 6;

                        }; // if(CMD61)
                        //*/
                        // sleep(1);
                    }
                    break; // case 8

                default:
                    {
                        printf("UNKNOWN RID=%d - EXIT FROM CLIENT! \n", RID);
                        FINE = true;

                    }; // default:

                }; // switch (RID)

            // вызов общего потока вывода
            // cout<<"user_bus_tx_thread"<<endl;
            // user_bus_tx_thread();
            // exit(100);

            steps++;
            if (steps > 1000000)
                steps = 0; // защита от переполнения

            // sleep(2);

        }; // while()  Главный конвейер
    // ГЛАВНЫЙ КОНВЕЙЕР++++++++++++++++++++++++++++++++++++++++++++++++++

    SocCAN.stop();

    return 0;
}
